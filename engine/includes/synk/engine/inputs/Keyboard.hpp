#pragma once

////////////////////
////////////////////
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
////////////////////
#include <synk/engine/Fwd.hpp>
#include <synk/engine/inputs/Keys.hpp>

////////////////////
namespace synk::engine
{
    class Keyboard : public storm::core::NonCopyable
    {
        public:

            Keyboard    (const Window& window);
            ~Keyboard   ();

            bool    contain     (const std::vector<int>& keys,
                                 const int& key)  const   noexcept;

            void    update      ()  noexcept;

            bool    getKey      (const int& key)    const   noexcept;
            bool    getKeyDown  (const int& key)    const   noexcept;
            bool    getKeyUp    (const int& key)    const   noexcept;

            static void keyCallback(GLFWwindow* window,
                                    int key,
                                    int scancode,
                                    int action,
                                    int mods);

        private:

            WindowConstObserverPtr  m_window;

            std::vector<int>        m_current_keys,
                                    m_down_keys,
                                    m_up_keys;
    };
};
