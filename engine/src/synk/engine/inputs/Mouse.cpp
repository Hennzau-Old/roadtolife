#include <synk/engine/inputs/Mouse.hpp>
#include <synk/engine/Window.hpp>

////////////////////
////////////////////
using namespace synk::engine;

bool buttons[5];

float pos_x;
float pos_y;

Mouse::Mouse(const Window& window)
:
    m_window    { &window }
{
    glfwSetMouseButtonCallback(m_window->getGLFWWindow(), buttonCallback);
    glfwSetCursorPosCallback(m_window->getGLFWWindow(), positionCallback);

    pos_x   =   0;
    pos_y   =   0;

    m_current_pos   =   glm::vec2(0, 0);
    m_mouse_vel     =   glm::vec2(0, 0);
}

Mouse::~Mouse()
{

}

bool Mouse::contain(const std::vector<int>& buttons, const int& button) const noexcept
{
    return std::find(buttons.begin(), buttons.end(), button) != buttons.end();
}

void Mouse::update()    noexcept
{
    if (m_grabed)
    {
        glfwSetInputMode(m_window->getGLFWWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    } else
    {
        glfwSetInputMode(m_window->getGLFWWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }

    m_mouse_vel   =   glm::vec2(pos_x - m_current_pos.x, pos_y - m_current_pos.y);
    m_current_pos =   glm::vec2(pos_x, pos_y);

    m_up_buttons.clear();

    for (auto i { 0u }; i < 5; i++)
    {
        if (!getButton(i) && contain(m_current_buttons, i))
        {
            m_up_buttons.emplace_back(i);
        }
    }

    m_down_buttons.clear();

    for (auto i { 0u }; i < 5; i++)
    {
        if (getButton(i) && !contain(m_current_buttons, i))
        {
            m_down_buttons.emplace_back(i);
        }
    }

    m_current_buttons.clear();

    for (auto i { 0u }; i < 5; i++)
    {
        if (getButton(i))
        {
            m_current_buttons.emplace_back(i);
        }
    }
}

bool Mouse::getButton(const int& button)    const  noexcept
{
    return buttons[button];
}

bool Mouse::getButtonDown(const int &button)    const   noexcept
{
    return contain(m_down_buttons, button);
}

bool Mouse::getButtonUp(const int &button)  const   noexcept
{
    return contain(m_up_buttons, button);
}

void Mouse::buttonCallback(GLFWwindow *window, int button, int action, int mods)
{
    buttons[button] =   (action == GLFW_PRESS);
}

void Mouse::positionCallback(GLFWwindow *window, double xpos, double ypos)
{
    pos_x   =   static_cast<float>(glm::floor(xpos));
    pos_y   =   static_cast<float>(glm::floor(ypos));
}
