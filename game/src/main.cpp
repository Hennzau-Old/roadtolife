#include <iostream>

///////////////////////
///////////////////////
#include <storm/log/LogHandler.hpp>

///////////////////////
///////////////////////
#include <game/Game.hpp>

///////////////////////
///////////////////////

#include <synk/engine/Window.hpp>
///////////////////////
///////////////////////

using namespace synk::engine;

///////////////////////
using storm::log::operator""_module;
static constexpr auto LOG_MODULE    =   "main"_module;

int main()
{
    auto    game    { std::make_unique<Game> () };

    /* loop */

    const auto  tick_time       { 1.0f / 60.0f };

    auto    last_tick_time      { glfwGetTime() };
    auto    accumulated_time    { 0.0f };
    auto    now_time            { last_tick_time };
    auto    timer               { last_tick_time };

    auto    ticks               { 0u };
    auto    frames              { 0u };

    while (!game->isClosed())
    {
        now_time            =   glfwGetTime();
        accumulated_time    +=  (now_time - last_tick_time) / tick_time;
        last_tick_time      =   now_time;

        while (accumulated_time     >= 1.0f)
        {
            ticks++;
            accumulated_time--;

            game->update();
        }

        frames++;
        game->render();

        if (now_time - timer > 1.0)
        {
            storm::log::LogHandler::ilog(LOG_MODULE, "FPS : {} | TPS : {} ", frames, ticks);

            timer   +=  1;
            frames  =   0;
            ticks   =   0;
        }
    }

    return 0;
}
