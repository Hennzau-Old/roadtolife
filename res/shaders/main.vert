#version 420 core

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec2 in_coords;

layout (std140, binding = 2) uniform Camera
{
    mat4    projection;
    mat4    view;
};

layout (std140, binding = 3) uniform Model
{
    mat4    model;
};

out vec2    pass_coords;

void main()
{
    pass_coords =   in_coords;

    gl_Position = projection * view * model * vec4(in_position, 0.0, 1.0);
}

