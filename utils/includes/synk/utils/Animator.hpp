#pragma once

#include <storm/core/NonCopyable.hpp>

namespace synk::utils
{
    class Animator : public storm::core::NonCopyable
    {
        public:

            Animator    (const std::uint32_t&   length,
                         const std::uint32_t&   speed,
                         const bool&            loop);
            ~Animator   ();

            inline void play()  noexcept
            {
                m_playing   =   true;
            }

            inline void play(const std::uint32_t& dir)  noexcept
            {
                m_dir       =   dir;
                m_playing   =   true;
            }

            inline void pause() noexcept
            {
                m_playing   =   false;
            }

            inline void stop()  noexcept
            {
                pause();
                m_frame     =   0;
            }

            inline void update  ()  noexcept
            {
                if (m_playing)
                {
                    m_time++;

                    if (m_time > m_speed)
                    {
                        m_frame++;

                        if (m_frame >= m_length)
                        {
                            if (m_loop)
                            {
                                m_frame =   0;
                            } else
                            {
                                m_frame =   m_length - 1;
                            }
                        }

                        m_time  =   0;
                    }
                }
            }

            inline auto&    isPlaying       ()  const   noexcept    { return m_playing; }
            inline auto&    getCurrentFrame ()  const   noexcept    { return m_frame; }
            inline auto&    getDir          ()  const   noexcept    { return m_dir; }

        private:

            std::uint32_t   m_length    =   0;
            std::uint32_t   m_speed     =   0;
            std::uint32_t   m_frame     =   0;
            std::uint32_t   m_time      =   0;
            std::uint32_t   m_dir       =   1;

            bool            m_loop      =   false;
            bool            m_playing   =   false;
    };
}
