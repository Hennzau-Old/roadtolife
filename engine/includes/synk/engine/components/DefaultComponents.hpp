#pragma once

////////////////////
////////////////////
#include <glm/glm.hpp>

////////////////////
////////////////////
#include <synk/engine/scene/Fwd.hpp>
#include <synk/engine/scene/StaticMesh.hpp>

////////////////////
namespace synk::engine
{
    namespace DefaultsComponents
    {
        struct Position
        {
            float x;
            float y;
            float z;
        };

        struct Rotation
        {
            float x;
            float y;
            float z;
        };

        struct Velocity
        {
            float x;
            float y;
            float z;
        };

        struct Size
        {
            float x;
            float y;
            float z;
        };

        struct Inertia
        {
            float coeff;
        };

        struct Model
        {
            glm::mat4   matrix  =   glm::mat4(1.0f);
        };

        struct Camera
        {
            glm::mat4   projection  =   glm::mat4(1.0f);
            glm::mat4   view        =   glm::mat4(1.0f);
        };

        struct StaticRenderObject
        {
            StaticMeshOwnedPtr  mesh;
            bool                render  =   true;
        };

        struct StaticTextObject
        {
            std::string text;
            std::string last_text;
        };

        struct Controllable
        {
            std::function<void(Position&, Velocity&, Rotation&)>    function;

            bool    value   =   true;
        };

        struct Collision
        {
            std::function<bool(const Position&, const Velocity&, const Size&)>  function;

            bool    value   =   true;
        };
    };
};
