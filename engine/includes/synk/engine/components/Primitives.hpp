#pragma once

#include <storm/core/NonCopyable.hpp>

#include <vector>
#include <array>
#include <glm/glm.hpp>

namespace synk::engine
{
    class Primitives : public storm::core::NonCopyable
    {
        public:

            struct  Primitive
            {
                std::vector<float>          vertices;
                std::vector<unsigned int>   indices;
                std::vector<std::uint8_t>   data_formats;
            };

            static inline Primitive getSquare   (const glm::vec2& pos, const glm::vec2& size, const glm::vec4& texture_coords) noexcept
            {
                auto    result  { Primitive {} };

                result.vertices =   {
                    pos.x,              pos.y,                  (0 + texture_coords.x) / texture_coords.z, (0 + texture_coords.y) / texture_coords.w,
                    pos.x   +   size.x, pos.y,                  (1 + texture_coords.x) / texture_coords.z, (0 + texture_coords.y) / texture_coords.w,
                    pos.x   +   size.x, pos.y   +   size.y,     (1 + texture_coords.x) / texture_coords.z, (1 + texture_coords.y) / texture_coords.w,
                    pos.x,              pos.y   +   size.y,     (0 + texture_coords.x) / texture_coords.z, (1 + texture_coords.y) / texture_coords.w
                };

                result.indices  =   {
                    0, 1, 2,
                    2, 3, 0
                };

                result.data_formats =   {
                    2, 2
                };

                return result;
            }

            static inline Primitive getSquare   (const glm::vec2& pos, const glm::vec2& size, const glm::vec4& colors, const glm::vec4& texture_coords) noexcept
            {
                auto    result  { Primitive {} };

                result.vertices =   {
                    pos.x,              pos.y,                  colors.x, colors.y, colors.z, colors.w,     (0 + texture_coords.x) / texture_coords.z, (0 + texture_coords.y) / texture_coords.w,
                    pos.x   +   size.x, pos.y,                  colors.x, colors.y, colors.z, colors.w,     (1 + texture_coords.x) / texture_coords.z, (0 + texture_coords.y) / texture_coords.w,
                    pos.x   +   size.x, pos.y   +   size.y,     colors.x, colors.y, colors.z, colors.w,     (1 + texture_coords.x) / texture_coords.z, (1 + texture_coords.y) / texture_coords.w,
                    pos.x,              pos.y   +   size.y,     colors.x, colors.y, colors.z, colors.w,     (0 + texture_coords.x) / texture_coords.z, (1 + texture_coords.y) / texture_coords.w
                };

                result.indices  =   {
                    0, 1, 2,
                    2, 3, 0
                };

                result.data_formats =   {
                    2, 4, 2
                };

                return result;
            }

            static inline Primitive getSquare   (const glm::vec2& pos, const glm::vec2& size, const float& color_factor, const glm::vec4& texture_coords) noexcept
            {
                auto    result  { Primitive {} };

                result.vertices =   {
                    pos.x,              pos.y,                  color_factor,       (0 + texture_coords.x) / texture_coords.z, (0 + texture_coords.y) / texture_coords.w,
                    pos.x   +   size.x, pos.y,                  color_factor,       (1 + texture_coords.x) / texture_coords.z, (0 + texture_coords.y) / texture_coords.w,
                    pos.x   +   size.x, pos.y   +   size.y,     color_factor,       (1 + texture_coords.x) / texture_coords.z, (1 + texture_coords.y) / texture_coords.w,
                    pos.x,              pos.y   +   size.y,     color_factor,       (0 + texture_coords.x) / texture_coords.z, (1 + texture_coords.y) / texture_coords.w
                };

                result.indices  =   {
                    0, 1, 2,
                    2, 3, 0
                };

                result.data_formats =   {
                    2, 1, 2
                };

                return result;
            }
    };
}
