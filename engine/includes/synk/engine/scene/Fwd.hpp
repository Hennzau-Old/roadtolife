#pragma once

#include <storm/core/Memory.hpp>

namespace synk::engine
{
    class Shaders;
    DECLARE_PTR_AND_REF(Shaders)

    class Texture;
    DECLARE_PTR_AND_REF(Texture)

    class StaticMesh;
    DECLARE_PTR_AND_REF(StaticMesh)

    class Scene;
    DECLARE_PTR_AND_REF(Scene)

    class UniformBuffer;
    DECLARE_PTR_AND_REF(UniformBuffer)
}
