#include <game/rooms/Builder.hpp>

#include <synk/engine/Engine.hpp>
#include <synk/engine/scene/Scene.hpp>
#include <synk/engine/inputs/Keyboard.hpp>
#include <synk/engine/inputs/Mouse.hpp>

#include <synk/entities/EntityManager.hpp>

#include <game/rooms/RoomManager.hpp>
#include <game/rooms/Tile.hpp>
#include <game/rooms/Room.hpp>

Builder::Builder(const synk::engine::Engine& engine, synk::engine::Scene& scene, RoomManager& room_manager)
:
    m_engine        { &engine },
    m_scene         { &scene },
    m_rooms         { &room_manager }
{
    m_keyboard  =   m_engine->getKeyboard();
    m_mouse     =   m_engine->getMouse();
    m_world     =   m_scene->getEntityManager();
}

Builder::~Builder()
{

}

void Builder::update()  noexcept
{
    if (m_keyboard->getKeyDown(KEY_LEFT))
    {
        m_rooms->previous();
    }

    if (m_keyboard->getKeyDown(KEY_RIGHT))
    {
        m_rooms->next();
    }

    if (m_mouse->getButton(1))
    {
        const auto  real_pos    { m_mouse->getPos() / m_engine->getWindow()->getResizedFactor() };

        const auto  x   { static_cast<int> (real_pos.x) / Tile::SIZE };
        const auto  y   { static_cast<int> (real_pos.y) / Tile::SIZE };

        const auto  temp_tile   { m_rooms->getCurrentRoom().getTile(x, y) };
        m_rooms->getCurrentRoom().setTile(x, y, { Tile::TILE_TYPE::TILE_PURPLE, true });

        const auto& pos { m_world->get<synk::engine::DefaultsComponents::Position> (m_rooms->getPlayer()) };

        if (!m_rooms->collide(glm::vec2(pos.x, pos.y), glm::vec2(), glm::vec2(Tile::SIZE, Tile::SIZE)))
        {
            m_rooms->getCurrentRoom().update();
        } else
        {
            m_rooms->getCurrentRoom().setTile(x, y, temp_tile);
            m_rooms->getCurrentRoom().update();
        }
    }

    if (m_mouse->getButton(0))
    {
        const auto  real_pos    { m_mouse->getPos() / m_engine->getWindow()->getResizedFactor() };

        const auto  x   { static_cast<int> (real_pos.x) / Tile::SIZE };
        const auto  y   { static_cast<int> (real_pos.y) / Tile::SIZE };

        m_rooms->getCurrentRoom().setTile(x, y, { Tile::TILE_TYPE::TILE_NO, false });
        m_rooms->getCurrentRoom().update();
    }
}
