#include <game/Game.hpp>

///////////////////////
///////////////////////
#include <storm/log/LogHandler.hpp>

///////////////////////
///////////////////////
#include <synk/engine/Synk.hpp>
#include <synk/engine/Window.hpp>
#include <synk/engine/Engine.hpp>
#include <synk/engine/inputs/Keyboard.hpp>
#include <synk/engine/inputs/Mouse.hpp>
#include <synk/engine/components/DefaultComponents.hpp>
#include <synk/engine/components/Primitives.hpp>
#include <synk/engine/scene/Scene.hpp>
#include <synk/engine/scene/StaticMesh.hpp>
#include <synk/engine/scene/Shaders.hpp>
#include <synk/engine/scene/Texture.hpp>
#include <synk/engine/scene/UniformBuffer.hpp>
#include <synk/engine/guis/GUIManager.hpp>

///////////////////////
///////////////////////
#include <synk/entities/EntityManager.hpp>

///////////////////////
///////////////////////
#include <glm/gtc/matrix_transform.hpp>

///////////////////////
///////////////////////
#include <game/rooms/Room.hpp>
#include <game/rooms/RoomManager.hpp>

///////////////////////
using namespace synk::engine;

///////////////////////
using storm::log::operator""_module;
static constexpr auto LOG_MODULE    =   "Game"_module;

#include <synk/engine/guis/FontManager.hpp>

///////////////////////
Game::Game()
{
    storm::log::LogHandler::setupDefaultLogger();
    storm::log::LogHandler::ilog(LOG_MODULE, "Starting RoadToLife using {} : {}", Synk::ENGINE_NAME, Synk::VERSION);

    const auto  window_settings { Window::CreateInfo {
            .width  { 1280u },
            .height { 704u },
            .title  { "RoadToLife" },
            .hints  {
                        { GLFW_CONTEXT_VERSION_MINOR,   3 },
                        { GLFW_CONTEXT_VERSION_MAJOR,   4 },
                        { GLFW_OPENGL_FORWARD_COMPAT,   GL_FALSE },
                        { GLFW_OPENGL_PROFILE,          GLFW_OPENGL_CORE_PROFILE},
                        { GLFW_RESIZABLE,               GLFW_FALSE }
                    }
    }};

    m_engine    =   std::make_unique<Engine>  (window_settings);
    m_window    =   m_engine->getWindow();
    storm::log::LogHandler::ilog(LOG_MODULE, "Creating GLFW Window : {}! Extent = {}w {}h", window_settings.title, window_settings.width, window_settings.height);

    glfwSwapInterval(1);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    m_keyboard  =   m_engine->getKeyboard();
    m_mouse     =   m_engine->getMouse();

    m_world         =   m_engine->createDefaultEntityManager();
    m_scene         =   m_engine->createScene(*m_world, 2, 3);
    m_camera        =   m_engine->create2DCamera(*m_world);
    m_gui_manager   =   m_engine->createGUIManager(0, 1);
    m_scene->setCamera(m_camera);

    // MENUS //

    m_start_button              =   std::make_unique<Texture> ("res/textures/guis/START.png");
    m_options_button            =   std::make_unique<Texture> ("res/textures/guis/OPTIONS.png");
    m_resume_button             =   std::make_unique<Texture> ("res/textures/guis/RESUME.png");
    m_quit_button               =   std::make_unique<Texture> ("res/textures/guis/QUIT.png");
    m_main_background_texture   =   std::make_unique<Texture> ("res/textures/guis/FOND.png");

    m_main_menu.components.insert(std::pair<std::string, entt::entity>      ("Background", m_gui_manager->createBackground(glm::vec2(1280, 704), *m_main_background_texture)));
    m_main_menu.components.insert(std::pair<std::string, entt::entity>      ("Start", m_gui_manager->createButton(glm::vec2(440, 300), glm::vec2(400, 100), *m_start_button)));
    m_main_menu.components.insert(std::pair<std::string, entt::entity>      ("Options", m_gui_manager->createButton(glm::vec2(440, 410), glm::vec2(400, 100), *m_options_button)));
    m_main_menu.components.insert(std::pair<std::string, entt::entity>      ("Quit", m_gui_manager->createButton(glm::vec2(440, 520), glm::vec2(400, 100), *m_quit_button)));
    m_gui_manager->stop(m_main_menu);

    FontManager::init();
    const auto  arial   { FontManager::load("res/fonts/arial.ttf") };

    m_in_game_menu.components.insert(std::pair<std::string, entt::entity>   ("Background", m_gui_manager->createBackground(glm::vec2(1280, 704), *m_main_background_texture)));
    m_in_game_menu.components.insert(std::pair<std::string, entt::entity>   ("Resume", m_gui_manager->createButton(glm::vec2(840, 444), glm::vec2(400, 100), *m_resume_button)));
    m_in_game_menu.components.insert(std::pair<std::string, entt::entity>   ("Quit", m_gui_manager->createButton(glm::vec2(840, 554), glm::vec2(400, 100), *m_quit_button)));
    m_in_game_menu.components.insert(std::pair<std::string, entt::entity>   ("Mode", m_gui_manager->createCheckBox(glm::vec2(100, 200), glm::vec2(50, 50), false)));
    m_in_game_menu.components.insert(std::pair<std::string, entt::entity>   ("ModeText", m_gui_manager->createText("BuilderMode", glm::vec2(160, 235), 0.5f, arial)));

    m_gui_manager->stop(m_in_game_menu);

    m_options_menu.components.insert(std::pair<std::string, entt::entity>   ("Background", m_gui_manager->createBackground(glm::vec2(1280, 704), *m_main_background_texture)));
    m_options_menu.components.insert(std::pair<std::string, entt::entity>   ("Quit", m_gui_manager->createButton(glm::vec2(840, 554), glm::vec2(400, 100), *m_quit_button)));
    m_options_menu.components.insert(std::pair<std::string, entt::entity>   ("VSYNC", m_gui_manager->createCheckBox(glm::vec2(100, 200), glm::vec2(50, 50), true)));
    m_options_menu.components.insert(std::pair<std::string, entt::entity>   ("VSynkText", m_gui_manager->createText("Vsync", glm::vec2(160, 235), 0.5f, arial)));
    m_gui_manager->stop(m_options_menu);

    setGameState(GAME_STATES::MAIN_MENU);

    m_status    =   true;
}

Game::~Game()
{

}

void Game::update() noexcept
{
    m_engine->update();

    if (m_window->wasResized())
    {
        glViewport(0, 0, m_window->window_info.width, m_window->window_info.height);
    }

    m_world->update();
    m_scene->update();

    manageGameStates();
    manageGameMenus();

    m_gui_manager->update();
    m_window->updateSize();

    /* Debug tools */

    if (m_keyboard->getKeyDown(KEY_F1))
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    if (m_keyboard->getKeyDown(KEY_F2))
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
}

void Game::render() const   noexcept
{
    m_window->clearBuffer();

        m_scene->render();

            m_world->render();

        m_gui_manager->render();

    m_window->swap();
}

void Game::manageGameStates()   noexcept
{
    if (m_game_state == GAME_STATES::GAME)
    {
        if (m_keyboard->getKeyDown(KEY_ESCAPE))
        {
            setGameState(GAME_STATES::IN_GAME_MENU);
            m_room_manager->stop();
        }

        m_room_manager->update();
    } else if (m_game_state == GAME_STATES::OPTIONS_MENU)
    {
        if (m_gui_manager->isCheck(m_options_menu.components["VSYNC"]))
        {
            glfwSwapInterval(1);
        } else
        {
            glfwSwapInterval(0);
        }

        if (m_gui_manager->isClicked(m_options_menu.components["Quit"]))
        {
            setGameState(GAME_STATES::MAIN_MENU);
        }
    } else if (m_game_state == GAME_STATES::IN_GAME_MENU)
    {
        if (m_gui_manager->isCheck(m_in_game_menu.components["Mode"]))
        {
            m_room_manager->changeMode(true);
        } else
        {
            m_room_manager->changeMode(false);
        }
    }
}

void Game::manageGameMenus()   noexcept
{
    if (m_gui_manager->isClicked(m_main_menu.components["Start"]))
    {
        m_room_manager  =   std::make_unique<RoomManager> (*m_engine, *m_scene);

        setGameState(GAME_STATES::GAME);
    }

    if (m_gui_manager->isClicked(m_main_menu.components["Quit"]))
    {
        m_status    =   false;
    }

    if (m_gui_manager->isClicked(m_main_menu.components["Options"]))
    {
        setGameState(GAME_STATES::OPTIONS_MENU);
    }

    if (m_gui_manager->isClicked(m_in_game_menu.components["Resume"]))
    {
        m_gui_manager->stop(m_in_game_menu);
        setGameState(GAME_STATES::GAME);
        m_room_manager->start();
    }

    if (m_gui_manager->isClicked(m_in_game_menu.components["Quit"]))
    {
        m_room_manager->stop();
        m_room_manager.reset(nullptr);
        setGameState(GAME_STATES::MAIN_MENU);
    }
}

void Game::setGameState(const GAME_STATES &state)   noexcept
{
    m_game_state    =   state;

    m_gui_manager->stop(m_main_menu);
    m_gui_manager->stop(m_options_menu);
    m_gui_manager->stop(m_in_game_menu);

    switch (m_game_state)
    {
        case GAME_STATES::MAIN_MENU:
        {
            m_gui_manager->start(m_main_menu);

            break;
        }
        case GAME_STATES::OPTIONS_MENU:
        {
            m_gui_manager->start(m_options_menu);

            break;
        }
        case GAME_STATES::IN_GAME_MENU:
        {
            m_gui_manager->start(m_in_game_menu);

            break;
        }
    }
}

bool Game::isClosed()   const   noexcept
{
    if (m_window->isClosed())   return true;
    if (!m_status)              return true;

    return false;
}
