#pragma once

////////////////////
////////////////////
#include <storm/core/Memory.hpp>

////////////////////
namespace synk::engine
{
    class Keyboard;
    DECLARE_PTR_AND_REF(Keyboard)

    class Mouse;
    DECLARE_PTR_AND_REF(Mouse)
}
