#include <synk/engine/scene/UniformBuffer.hpp>
#include <synk/engine/scene/Shaders.hpp>

////////////////////
using namespace synk::engine;

////////////////////
UniformBuffer::UniformBuffer(const std::string& name,
                             const std::uint32_t& size,
                             const std::uint32_t& binding)
:
    m_size      { size },
    m_binding   { binding },
    m_name      { name }
{

    glGenBuffers(1, &m_ubo);

    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    glBufferData(GL_UNIFORM_BUFFER, m_size, NULL, GL_DYNAMIC_COPY);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    glBindBufferBase(GL_UNIFORM_BUFFER, m_binding, m_ubo);
}

UniformBuffer::~UniformBuffer()
{
    glDeleteBuffers(1, &m_ubo);
}
