#pragma once

////////////////////
////////////////////
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <iostream>
#include <vector>

////////////////////
////////////////////

#include <storm/core/NonCopyable.hpp>
////////////////////
////////////////////
#include <synk/engine/Fwd.hpp>

////////////////////
namespace synk::engine
{
    class Window : public storm::core::NonCopyable
    {
        public:

            /* public structures */

            struct Hints
            {
                std::uint32_t       hint;
                std::uint32_t       value;
            };

        private:

            /* private structures */

            struct Info
            {
                std::uint32_t       initial_width;
                std::uint32_t       initial_height;
                std::uint32_t       last_width;
                std::uint32_t       last_height;
                std::uint32_t       width;
                std::uint32_t       height;
                std::string         title;
                std::vector<Hints>  hints;
            };

        public:

            /* structure */

            struct CreateInfo
            {
                std::uint32_t       width;
                std::uint32_t       height;
                std::string         title;
                std::vector<Hints>  hints;
            };

            /* functions */

            Window  (const CreateInfo& create_info);
            ~Window ();

            inline void             clearBuffer() const noexcept
            {
                glClear(GL_COLOR_BUFFER_BIT);
            }

            inline void             update      ()  const   noexcept
            {
                glfwPollEvents();
            }

            inline void             swap        ()  const   noexcept
            {
                glfwSwapBuffers(m_window);
            }

            inline bool             wasResized  ()   const  noexcept
            {
                if (window_info.last_width != window_info.width || window_info.last_height != window_info.height)
                {
                    return true;
                }

                return false;
            }

            inline void             updateSize  ()  noexcept
            {
                window_info.last_width  =   window_info.width;
                window_info.last_height =   window_info.height;
            }

            static void             framebufferResizedCallback  (GLFWwindow* window, int width, int height);

            inline auto             isClosed        () const noexcept { return glfwWindowShouldClose(m_window); }
            inline auto             getAspect       () const noexcept { return (float) window_info.width / (float) window_info.height; }
            inline auto             getResizedFactor() const noexcept { return glm::vec2((float) window_info.width / window_info.initial_width, (float) window_info.height / window_info.initial_height); }

            inline auto             getGLFWWindow   () const noexcept { return m_window; }

            /* variables */

            Info                    window_info;

        private:

            /* functions */

            /* variables */

            GLFWwindow*             m_window;
    };
};
