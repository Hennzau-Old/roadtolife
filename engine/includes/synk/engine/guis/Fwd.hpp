#pragma once

#include <storm/core/Memory.hpp>

namespace synk::engine
{
    class GUIManager;
    DECLARE_PTR_AND_REF(GUIManager)
}
