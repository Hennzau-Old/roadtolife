#pragma once

#include <storm/core/Memory.hpp>

namespace synk::engine
{
    class Window;
    DECLARE_PTR_AND_REF(Window)

    class Engine;
    DECLARE_PTR_AND_REF(Engine)
};
