#pragma once

namespace synk::engine
{
    namespace Synk
    {
        constexpr   auto    ENGINE_NAME     { "Synk-OpenGL" };
        constexpr   auto    VERSION         { "0.0.1" };
    };
};
