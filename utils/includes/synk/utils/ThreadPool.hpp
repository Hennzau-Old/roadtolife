#pragma once

#include <iostream>
#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <future>
#include <functional>

namespace synk::utils
{
    class ThreadPool
    {
        public:
        
            ThreadPool  (const std::uint32_t& count);
            ~ThreadPool ();

            template<class F, class... Args>
            inline  auto    exec    (F&& f, Args&&... args) noexcept    -> std::future<typename std::result_of<F(Args...)>::type>;
            inline  void    join    ()  noexcept;

            inline  auto&   getWorkers  ()          noexcept    { return m_workers; }
            inline  auto&   getWorkers  ()  const   noexcept    { return m_workers; }

            inline  auto&   getTasks    ()          noexcept    { return m_tasks; }
            inline  auto&   getTasks    ()  const   noexcept    { return m_tasks; }

            inline  auto&   getMutex    ()          noexcept    { return m_mutex; }
            inline  auto&   getMutex    ()  const   noexcept    { return m_mutex; }

        private:

            std::uint32_t                               m_count;
            std::vector<std::unique_ptr<std::thread>>   m_workers;
            std::unique_ptr<std::mutex>                 m_mutex;
            std::queue<std::function<void()>>           m_tasks;
            bool                                        m_status;
    };

    template<class F, class... Args>
    inline auto ThreadPool::exec(F&& f, Args&&... args) noexcept    -> std::future<typename std::result_of<F(Args...)>::type>
    {
        using   return_type = typename std::result_of<F(Args...)>::type;

        const   auto    task    {   std::make_shared<std::packaged_task<return_type()>>
                                (std::bind(std::forward<F>(f), std::forward<Args>(args)...)) };
                                
        auto            result  { task->get_future() };

        m_tasks.push( [task]() 
        {
            (*task) ();
        });

        return result;
    }

    inline void ThreadPool::join() noexcept
    {
        m_status    =   true;

        for (const auto& worker : m_workers)
        {
            worker->join();
        }
    }
};

