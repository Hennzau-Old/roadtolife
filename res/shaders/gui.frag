#version 420 core

out vec4    frag_color;

in vec2     pass_coords;
in float    pass_color;

uniform sampler2D   sampler_texture;

void main()
{
    frag_color  =   texture(sampler_texture, pass_coords) * pass_color;
}
