#pragma once

#include <string>

#include <glm/glm.hpp>

namespace synk::engine
{
    namespace GUIComponents
    {
        struct Button
        {
            bool    clicked;

            glm::vec2   size;
        };

        struct Slide
        {
            glm::vec2   rayon;
            glm::vec4   pos;
        };

        struct Text
        {
            std::string text;
            std::string last_text;
        };

        struct CheckBox
        {
            bool check_status;

            bool click_status = false;
        };

        struct Overlable
        {
            float   value   =   1.0f;
            bool    status  =   false;
        };

        struct Status
        {
            bool    status  =   false;
        };
    }
}
