#pragma once

////////////////////
////////////////////
#include <GL/glew.h>

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
////////////////////
#include <synk/engine/scene/Fwd.hpp>

////////////////////
namespace synk::engine
{
    class UniformBuffer : public storm::core::NonCopyable
    {
        public:

            UniformBuffer   (const std::string& name,
                             const std::uint32_t& size,
                             const std::uint32_t& binding);
            ~UniformBuffer  ();

            template<typename T>
            void        upload(T data, const std::uint32_t& size, const std::uint32_t& offset = 0)
            {
                glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);

                    glBufferSubData(GL_UNIFORM_BUFFER, offset, size, &data);

                glBindBuffer(GL_UNIFORM_BUFFER, 0);
            }

            inline auto     getID       ()  const   noexcept    { return m_ubo; }
            inline auto     getSize     ()  const   noexcept    { return m_size; }
            inline auto     getBinding  ()  const   noexcept    { return m_binding; }
            inline auto     getName     ()  const   noexcept    { return m_name; }

        private:

            GLuint          m_ubo;
            std::uint32_t   m_size;
            std::uint32_t   m_binding;
            std::string     m_name;
    };
}
