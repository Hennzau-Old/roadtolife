#include <iostream>
#include <memory>

#include <synk/utils/File.hpp>
#include <synk/utils/ThreadPool.hpp>

using namespace synk::utils;

int main()
{
    std::unique_ptr<ThreadPool>     m_thread_pool;
    std::vector<std::future<int>>   m_results;

    m_thread_pool   =   std::make_unique<ThreadPool>(4);

    for (auto i { 0 }; i < 10; i++)
    {
        m_results.emplace_back(m_thread_pool->exec([](const int& value)
        {
            return value;
        }, i));
    }

    m_thread_pool->join();

    for (auto& result : m_results)
    {
        std::cout << result.get() << std::endl;
    }

    return 0;
}