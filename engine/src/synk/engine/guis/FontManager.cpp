#include <synk/engine/guis/FontManager.hpp>
#include <synk/engine/scene/Texture.hpp>

#include <storm/log/LogHandler.hpp>

#include <GL/glew.h>

using namespace synk::engine;

using storm::log::operator""_module;
static constexpr auto LOG_MODULE    =   "FontManager"_module;

FT_Library                                  library;
std::map<std::string, FontManager::Font>    fonts;
std::vector<TextureOwnedPtr>                textures;

#undef max
#undef min

void FontManager::init()    noexcept
{
    if (FT_Init_FreeType(&library))
    {
        storm::log::LogHandler::flog(LOG_MODULE, "Could not init FreeType Library");
    }
}

FontManager::Font FontManager::load(const std::string& font) noexcept
{
    const auto  it  { fonts.find(font) };

    if (it != fonts.end())
    {
        storm::log::LogHandler::ilog(LOG_MODULE, "Font already load");
        return it->second;
    }

    FT_Face face;
    if (FT_New_Face(library, font.c_str(), 0, &face))
    {
        storm::log::LogHandler::flog("Failed to load font {}", font);
    }

    FontManager::Font   result;

    auto    width   { 0u };
    auto    height  { 0u };

    FT_Set_Pixel_Sizes(face, 0, 48);

    for (auto i { 32 }; i < 128; i++)
    {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER))
        {
            storm::log::LogHandler::elog(LOG_MODULE, "Loading character {} failed", i);
            continue;
        }

        width   +=  face->glyph->bitmap.width;
        height  =   std::max(height, face->glyph->bitmap.rows);
    }

    result.size =   glm::vec2(width, height);

    GLuint  texture_ID;
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glGenTextures(1, &texture_ID);
    glBindTexture(GL_TEXTURE_2D, texture_ID);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, 0);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    auto    x   { 0 };

    for (int i { 32 }; i < 128; i++)
    {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER))
        {
            storm::log::LogHandler::elog(LOG_MODULE, "Loading character {} failed", i);
            continue;
        }

        glTexSubImage2D(GL_TEXTURE_2D, 0, x, 0, face->glyph->bitmap.width, face->glyph->bitmap.rows, GL_ALPHA, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);

        const auto  character   { FontManager::Character
        {
            glm::vec2(face->glyph->advance.x >> 6, face->glyph->advance.y >> 6),
            glm::vec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::vec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            static_cast<float>(x)   /   width
        }};

        result.characters.insert(std::pair<char, FontManager::Character> (i, character));

        x += face->glyph->bitmap.width;
    }

    glBindTexture(GL_TEXTURE_2D, 0);

    auto    texture { std::make_unique<Texture> (texture_ID) };

    result.texture_font =   storm::core::makeConstObserver(texture);

    textures.emplace_back(std::move(texture));

    fonts[font] =   result;

    FT_Done_Face(face);

    return result;
}

glm::vec2 FontManager::fill(const char &character, const Font &font, const float &scale, std::vector<float> &vertices, std::vector<unsigned int> &indices, const glm::vec2 &offset, const std::uint32_t &count) noexcept
{
    const auto  it  { font.characters.find(character) };

    if (it == font.characters.end())
    {
        storm::log::LogHandler::elog(LOG_MODULE, "Character {} not found in the font");

        return glm::vec2();
    }

    const auto  s_character { it->second };

    const auto  x   { offset.x + s_character.pos.x * scale };
    const auto  y   { offset.y + s_character.pos.y * scale };
    const auto  w   { s_character.size.x * scale };
    const auto  h   { s_character.size.y * scale };

    if (!w || !h)
    {
        return glm::vec2();
    }

    std::vector<float>  t_vertices
    {
        x,      -y,         1.0f,      s_character.offset,                                         0,
        x + w,  -y,         1.0f,      s_character.offset + s_character.size.x / font.size.x,      0,
        x + w,  -y + h,     1.0f,      s_character.offset + s_character.size.x / font.size.x,      s_character.size.y / font.size.y,
        x,      -y + h,     1.0f,      s_character.offset,                                         s_character.size.y / font.size.y
    };

    std::vector<unsigned int>   t_indices
    {
        0 + 4 * count, 1 + 4 * count, 2 + 4 * count,
        2 + 4 * count, 3 + 4 * count, 0 + 4 * count
    };

    vertices.insert(vertices.end(), t_vertices.begin(), t_vertices.end());
    indices.insert(indices.end(), t_indices.begin(), t_indices.end());

    return glm::vec2(s_character.advance.x, s_character.advance.y) * scale;
}
