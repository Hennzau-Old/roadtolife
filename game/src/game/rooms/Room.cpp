#include <game/rooms/Room.hpp>
#include <game/rooms/Dimension.hpp>

////////////////////
////////////////////
#include <synk/engine/Engine.hpp>
#include <synk/engine/Window.hpp>
#include <synk/engine/components/DefaultComponents.hpp>
#include <synk/engine/scene/StaticMesh.hpp>
#include <synk/engine/inputs/Keyboard.hpp>
#include <synk/engine/inputs/Mouse.hpp>

////////////////////
////////////////////
#include <synk/entities/EntityManager.hpp>

////////////////////
Room::Room(const synk::engine::Engine& engine,
           synk::entities::EntityManager& world,
           const glm::vec2& size)
:
    m_engine    { &engine },
    m_world     { &world },
    m_size      { size }
{
    m_keyboard  =   m_engine->getKeyboard();
    m_mouse     =   m_engine->getMouse();

    m_dimensions[0] =   std::make_unique<Dimension> (engine, world, m_size);
    m_dimensions[1] =   std::make_unique<Dimension> (engine, world, m_size);

    m_current_dimension =   0;

    stop();
}

Room::~Room()
{
    m_world->removeAll(m_dimensions[0]->getMesh());
    m_world->destroy(m_dimensions[0]->getMesh());
    m_world->removeAll(m_dimensions[1]->getMesh());
    m_world->destroy(m_dimensions[1]->getMesh());
}

void Room::changeDimension(const bool &dim) noexcept
{
    auto&   render_object_one   { m_world->get<synk::engine::DefaultsComponents::StaticRenderObject> (m_dimensions[dim]->getMesh()) };
    render_object_one.render    =   true;

    auto&   render_object_two   { m_world->get<synk::engine::DefaultsComponents::StaticRenderObject> (m_dimensions[!dim]->getMesh()) };
    render_object_two.render    =   false;

    m_current_dimension =   dim;
}

void Room::swap()   noexcept
{
    changeDimension(!m_current_dimension);
}

void Room::stop ()  noexcept
{
    auto&   render_object_one   { m_world->get<synk::engine::DefaultsComponents::StaticRenderObject> (m_dimensions[0]->getMesh()) };
    render_object_one.render    =   false;

    auto&   render_object_two   { m_world->get<synk::engine::DefaultsComponents::StaticRenderObject> (m_dimensions[1]->getMesh()) };
    render_object_two.render    =   false;
}

void Room::start()  noexcept
{
    changeDimension(0);
}

void Room::update() noexcept
{
    m_dimensions[m_current_dimension]->update();
}

Tile::TileStructure Room::getTile(const int &x, const int &y)   const   noexcept
{
    return getTile(m_current_dimension, x, y);
}

Tile::TileStructure Room::getTile(const bool& dimension, const int &x, const int &y)   const   noexcept
{
    return m_dimensions[dimension]->getTile(x, y);
}

void Room::setTile(const bool &dimension, const int &x, const int &y, const Tile::TileStructure &tile)  noexcept
{
    m_dimensions[dimension]->setTile(x, y, tile);
}

void Room::setTile(const int &x, const int &y, const Tile::TileStructure &tile) noexcept
{
    setTile(m_current_dimension, x, y, tile);
}
