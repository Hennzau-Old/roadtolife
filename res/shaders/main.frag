#version 420 core

out vec4    frag_color;

in vec2     pass_coords;

uniform sampler2D   sampler_texture;

void main()
{
    frag_color  =   texture(sampler_texture, pass_coords);
}