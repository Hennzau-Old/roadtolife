#pragma once

////////////////////
////////////////////
#include <GL/glew.h>
#include <map>

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
////////////////////
#include <synk/engine/scene/Fwd.hpp>
#include <synk/entities/Fwd.hpp>

////////////////////
namespace synk::engine
{
    class Scene : public storm::core::NonCopyable
    {
        public:

            Scene   (entities::EntityManager& world,
                     const std::uint32_t& camera_binding,
                     const std::uint32_t& model_binding);
            ~Scene  ();

            void                setCamera       (const entt::entity& camera)    noexcept;

            void                update          ()          noexcept;
            void                render          ()  const   noexcept;

            inline auto&        getEntityManager()          noexcept    { return m_world; }
            inline auto&        getEntityManager()  const   noexcept    { return m_world; }

            void                addStaticMesh   (const entt::entity& static_mesh,
                                                 const Shaders& shaders,
                                                 const Texture& texture,
                                                 const bool& initialize_shader = true) noexcept;

            void                removeStaticMesh(const entt::entity& static_mesh,
                                                 const Shaders& shaders,
                                                 const Texture& texture)   noexcept;

            void                removeStaticMesh(const entt::entity& static_mesh)   noexcept;

        private:

            struct  TextureObjects
            {
                TextureConstObserverPtr texture;

                std::vector<entt::entity> meshes;
            };

            struct  RenderObjects
            {
                ShadersConstObserverPtr shaders;

                std::vector<TextureObjects> objects;
            };

            std::vector<RenderObjects>  m_static_meshes;

            UniformBufferOwnedPtr   m_camera_buffer;
            UniformBufferOwnedPtr   m_model_buffer;

            entities::EntityManagerObserverPtr      m_world;
            entt::entity                            m_camera_target;
    };
}
