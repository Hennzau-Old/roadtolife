#include <synk/engine/Window.hpp>

////////////////////
using namespace synk::engine;

////////////////////
Window::Window(const Window::CreateInfo& create_info)
{
    window_info.initial_width   =   create_info.width;
    window_info.initial_height  =   create_info.height;
    window_info.width           =   create_info.width;
    window_info.height          =   create_info.height;
    window_info.last_width      =   create_info.width;
    window_info.last_height     =   create_info.height;
    window_info.title           =   create_info.title;
    window_info.hints           =   create_info.hints;

    if (!glfwInit())
    {
        throw std::runtime_error("glfwInit failed!");
    }

    for (const auto hint : window_info.hints)
    {
        glfwWindowHint(hint.hint, hint.value);
    }

    m_window = glfwCreateWindow(window_info.width, window_info.height, window_info.title.c_str(), nullptr, nullptr);

    if (!m_window)
    {
        throw std::runtime_error("glfwCreateWindow failed!");
    }

    glfwMakeContextCurrent(m_window);
    glewExperimental = true;

    if (glewInit() != GLEW_OK)
    {
        throw std::runtime_error("glewInit failed!");
    }

    glfwSetWindowUserPointer(m_window, this);
    glfwSetFramebufferSizeCallback(m_window, framebufferResizedCallback);

    glfwSwapInterval(0);
}

Window::~Window()
{
    glfwDestroyWindow(m_window);
    glfwTerminate();
}

void Window::framebufferResizedCallback(GLFWwindow* window, int width, int height)
{
    auto display { reinterpret_cast<Window*> (glfwGetWindowUserPointer(window)) };

    display->window_info.width           = width;
    display->window_info.height          = height;
}
