#pragma once

#include <string>
#include <vector>
#include <array>
#include <map>
#include <glm/glm.hpp>

#include <synk/engine/scene/Fwd.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

namespace synk::engine
{
    class FontManager
    {
        public:

            struct Character
            {
                glm::vec2   advance;
                glm::vec2   size;
                glm::vec2   pos;

                float       offset;
            };

            struct Font
            {
                TextureConstObserverPtr     texture_font;
                glm::vec2                   size;
                std::map<char, Character>   characters;
            };

        public:

            static  void                    init()  noexcept;
            static  Font                    load(const std::string& font)   noexcept;

            static  glm::vec2               fill(const char&  character, const Font& font, const float& scale, std::vector<float>& vertices, std::vector<unsigned int>& indices, const glm::vec2& offset, const std::uint32_t& count)    noexcept;
    };
}
