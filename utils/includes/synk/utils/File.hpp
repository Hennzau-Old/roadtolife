#pragma once

#include <string>
#include <vector>
#include <fstream>

namespace synk::utils
{
    namespace File
    {
        inline static std::string readFile(const std::string& file)
        {
            auto    file_stream { std::ifstream(file, std::ios::ate | std::ios::binary) };

            if (!file_stream.is_open())
            {
                throw std::runtime_error("Error while opening the file : " + file + "!");
            }

            const auto  file_size   { std::uint32_t(file_stream.tellg()) };

            auto    buffer  { std::vector<char> (file_size) };

            file_stream.seekg(0);
            file_stream.read(buffer.data(), file_size);
            file_stream.close();

            return std::string(buffer.begin(), buffer.end());
        }
    };
};