#pragma once

#include <storm/core/NonCopyable.hpp>

#include <synk/engine/Fwd.hpp>
#include <synk/engine/scene/Fwd.hpp>
#include <synk/engine/inputs/Fwd.hpp>

#include <synk/entities/Fwd.hpp>

#include <synk/engine/guis/GUIComponents.hpp>
#include <synk/engine/guis/FontManager.hpp>

#include <glm/glm.hpp>

namespace synk::engine
{
    class GUIManager : public storm::core::NonCopyable
    {
        public:

            struct Interface
            {
                std::map<std::string, entt::entity> components;
            };

            GUIManager  (const Engine& engine,
                         const std::uint32_t& camera_binding,
                         const std::uint32_t& model_binding);
            ~GUIManager ();

            entt::entity    createButton    (const glm::vec2& pos,
                                             const glm::vec2& size,
                                             const Texture& texture)   noexcept;

            entt::entity    createSlider    (const glm::vec2& pos,
                                             const glm::vec2& size = glm::vec2(20, 40),
                                             const glm::vec2& rayon = glm::vec2())   noexcept;

            entt::entity    createText      (const std::string& text,
                                             const glm::vec2& pos,
                                             const float& scale,
                                             const FontManager::Font& font) noexcept;

            entt::entity    createCheckBox  (const glm::vec2& pos,
                                             const glm::vec2& size,
                                             const bool&      status = false) noexcept;

            entt::entity    createBackground(const glm::vec2& size,
                                             const Texture& texture) noexcept;

            bool            isClicked       (const entt::entity& button)    const   noexcept;
            glm::vec2       getValue        (const entt::entity& slider)    const   noexcept;
            bool            isCheck         (const entt::entity& check_box) const   noexcept;

            void            update          ()          noexcept;
            void            render          ()  const   noexcept;

            void            stop            (const Interface& _interface)    noexcept;
            void            start           (const Interface& _interface)    noexcept;

        private:

            EngineConstObserverPtr          m_engine;
            KeyboardConstObserverPtr        m_keyboard;
            MouseConstObserverPtr           m_mouse;

            entities::EntityManagerOwnedPtr m_manager;
            SceneOwnedPtr                   m_scene;
            ShadersOwnedPtr                 m_shaders;
            entt::entity                    m_camera;

            synk::engine::TextureOwnedPtr   m_slider_texture;
            synk::engine::TextureOwnedPtr   m_check_box_texture;
            synk::engine::TextureOwnedPtr   m_not_check_box_texture;
    };
}
