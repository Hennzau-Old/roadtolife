#pragma once

#include <storm/core/NonCopyable.hpp>

#include <synk/engine/Fwd.hpp>
#include <synk/engine/scene/Fwd.hpp>

#include <synk/entities/Fwd.hpp>

#include <game/rooms/Tile.hpp>

#include <glm/glm.hpp>

class Dimension : public storm::core::NonCopyable
{
    public:

        Dimension   (const synk::engine::Engine& engine,
                     synk::entities::EntityManager& world,
                     const glm::vec2& size);
        ~Dimension  ();

        void            create  ()  noexcept;
        void            update  ()  noexcept;

        inline auto&    getMesh ()          noexcept    { return m_mesh; }
        inline auto&    getMesh ()  const   noexcept    { return m_mesh; }

        inline Tile::TileStructure  getTile (const int& x, const int& y)    const   noexcept
        {
            if (x < 0 || y < 0 || x >= m_size.x || y >= m_size.y)
            {
                return { Tile::TILE_TYPE::TILE_NO, true};
            }

            return m_tiles[x][y];
        }

        inline void     setTile (const int& x, const int& y, const Tile::TileStructure& type)   noexcept
        {
            if (x < 0 || y < 0 || x >= m_size.x || y >= m_size.y)
            {
                return;
            }

            m_tiles[x][y]   =   type;
        }

    private:

        synk::engine::EngineConstObserverPtr            m_engine;
        synk::entities::EntityManagerObserverPtr        m_world;

        entt::entity                                    m_mesh;

        glm::vec2                                       m_size;

        std::vector<std::vector<Tile::TileStructure>>   m_tiles;
};
