#pragma once

////////////////////
////////////////////
#include <glm/glm.hpp>

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
////////////////////
#include <synk/engine/Window.hpp>
#include <synk/engine/Fwd.hpp>
#include <synk/engine/inputs/Fwd.hpp>
#include <synk/engine/scene/Fwd.hpp>
#include <synk/engine/guis/Fwd.hpp>

////////////////////
////////////////////
#include <synk/entities/Fwd.hpp>

////////////////////
namespace synk::engine
{
    class Engine : public storm::core::NonCopyable
    {
        public:

            Engine  (const Window::CreateInfo& window_settings);
            ~Engine ();

            void                                        update()    noexcept;

            entities::EntityManagerOwnedPtr             createEntityManager         ()                                      const   noexcept;
            entities::EntityManagerOwnedPtr             createDefaultEntityManager  ()                                      const   noexcept;
            GUIManagerOwnedPtr                          createGUIManager            (const std::uint32_t& camera_binding,
                                                                                     const std::uint32_t& model_binding)    const   noexcept;

            SceneOwnedPtr                               createScene                 (entities::EntityManager& entity_manager,
                                                                                     const std::uint32_t& camera_binding,
                                                                                     const std::uint32_t& model_binding)    const   noexcept;

            entt::entity                                createCamera                (entities::EntityManager& entity_manager,
                                                                                     const glm::vec3& pos = glm::vec3(0.0f),
                                                                                     const glm::vec3& rot = glm::vec3(0.0f)) const   noexcept;

            entt::entity                                create2DCamera              (entities::EntityManager& entity_manager,
                                                                                     const glm::vec3& pos = glm::vec3(0.0f),
                                                                                     const glm::vec3& rot = glm::vec3(-90.0f, 0.0f, 0.0f)) const   noexcept;

            entt::entity                                createStaticMesh            (entities::EntityManager& entity_manager,
                                                                                     const std::vector<float>& vertices,
                                                                                     const std::vector<unsigned int>& indices,
                                                                                     const std::vector<std::uint8_t>& data_formats,
                                                                                     const glm::vec3& pos = glm::vec3(0.0f),
                                                                                     const glm::vec3& rot = glm::vec3(0.0f))    const   noexcept;

            entt::entity                                createStaticText            (entities::EntityManager& entity_manager,
                                                                                     const std::string& text,
                                                                                     const std::string& font,
                                                                                     const glm::vec3& pos = glm::vec3(0.0f),
                                                                                     const glm::vec3& rot = glm::vec3(0.0f))    const   noexcept;

            inline auto                                 getWindow                   ()          noexcept    { return storm::core::makeObserver(m_window); }
            inline auto                                 getWindow                   ()  const   noexcept    { return storm::core::makeConstObserver(m_window); }

            inline auto                                 getKeyboard                 ()          noexcept    { return storm::core::makeObserver(m_keyboard); }
            inline auto                                 getKeyboard                 ()  const   noexcept    { return storm::core::makeConstObserver(m_keyboard); }

            inline auto                                 getMouse                    ()          noexcept    { return storm::core::makeObserver(m_mouse); }
            inline auto                                 getMouse                    ()  const   noexcept    { return storm::core::makeConstObserver(m_mouse); }

        private:

            WindowOwnedPtr      m_window;
            KeyboardOwnedPtr    m_keyboard;
            MouseOwnedPtr       m_mouse;
    };
}
