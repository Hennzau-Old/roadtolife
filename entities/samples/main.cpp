#include <iostream>
#include <memory>

#include <synk/entities/EntityManager.hpp>
#include <synk/utils/File.hpp>

struct Position
{
    float   x;
    float   y;
    float   z;
};

int main()
{
    std::cout << "Hello World !\nThis is the entities module! It aims to implement an EntityManager with EnTT" << std::endl;
    
    auto    manager { std::make_unique<synk::entities::EntityManager>() };

    auto    entity_test { manager->make() };
    manager->addComponent<Position>(entity_test, 0.0f, 5.0f, 0.0f);

//    manager->addUpdate([](const entt::registry& registry)
//    {
//        registry.view<Position>().each([](auto& pos)
//        {
//            pos.x += 1;
//            pos.y += 1;
//        });
//    });

    return EXIT_SUCCESS;
}

