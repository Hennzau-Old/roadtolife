#include <synk/engine/scene/StaticMesh.hpp>

#include <iostream>

////////////////////
////////////////////
using namespace synk::engine;

////////////////////
StaticMesh::StaticMesh(const std::vector<float>& vertices, const std::vector<unsigned int>& indices, const std::vector<std::uint8_t>& data_formats)
:
    m_vertices      { vertices },
    m_indices       { indices },
    m_data_formats  { data_formats }
{
    auto stride { 0u };

    for (const auto& data_format : data_formats)
    {
        stride += data_format * sizeof(float);
    }

    glGenVertexArrays(1, &m_vao);
    glGenBuffers(1, &m_vbo);
    glGenBuffers(1, &m_ibo);

    glBindVertexArray(m_vao);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

            auto    offset  { 0u };

            for (auto i { 0u }; i < data_formats.size(); i++)
            {
                glEnableVertexAttribArray(i);
                glVertexAttribPointer(i, data_formats[i], GL_FLOAT, GL_FALSE, stride, reinterpret_cast<const void*> (offset));

                offset += data_formats[i] * sizeof(float);
            }

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    m_vertex_count  =   indices.size();
}

StaticMesh::StaticMesh(const StaticMesh& static_mesh)
:
    StaticMesh(static_mesh.getVertices(), static_mesh.getIndices(), static_mesh.getFormats())
{

}

StaticMesh::~StaticMesh()
{
    glDeleteVertexArrays(1, &m_vao);
    glDeleteBuffers(1, &m_vbo);
    glDeleteBuffers(1, &m_ibo);
}

void StaticMesh::update(const std::vector<float> &vertices, const std::uint32_t &offset)    const   noexcept
{
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

        glBufferSubData(GL_ARRAY_BUFFER, offset, vertices.size() * sizeof(float), vertices.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void StaticMesh::update(const std::vector<unsigned int>& indices, const std::uint32_t& offset)    noexcept
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, indices.size() * sizeof(unsigned int), indices.data());

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    m_vertex_count  =   indices.size();
}

void StaticMesh::render()   const   noexcept
{
    glBindVertexArray(m_vao);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

            glDrawElements(GL_TRIANGLES, m_vertex_count, GL_UNSIGNED_INT, NULL);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}
