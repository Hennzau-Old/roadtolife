#pragma once

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
////////////////////
#include <synk/engine/Fwd.hpp>
#include <synk/engine/scene/Fwd.hpp>
#include <synk/engine/inputs/Fwd.hpp>

////////////////////
////////////////////
#include <synk/entities/Fwd.hpp>

////////////////////
////////////////////
#include <game/rooms/Fwd.hpp>
#include <game/rooms/Dimension.hpp>

////////////////////
class Room : public storm::core::NonCopyable
{
    public:

        Room    (const synk::engine::Engine& engine,
                 synk::entities::EntityManager& world,
                 const glm::vec2& size);

        ~Room   ();

        void            update  ()  noexcept;

        void            changeDimension (const bool& dim)   noexcept;
        void            swap            ()                  noexcept;
        void            stop            ()  noexcept;
        void            start           ()  noexcept;

        void            setTile         (const bool& dimension,
                                         const int& x,
                                         const int& y,
                                         const Tile::TileStructure& tile)  noexcept;

        void            setTile         (const int& x,
                                         const int& y,
                                         const Tile::TileStructure& tile)  noexcept;


        Tile::TileStructure getTile     (const int& x, const int& y) const   noexcept;
        Tile::TileStructure getTile     (const bool& dimension, const int& x, const int& y) const   noexcept;

        inline auto&    getMeshOne  ()          noexcept    { return m_dimensions[0]->getMesh(); }
        inline auto&    getMeshOne  ()  const   noexcept    { return m_dimensions[0]->getMesh(); }
        inline auto&    getMeshTwo  ()          noexcept    { return m_dimensions[1]->getMesh(); }
        inline auto&    getMeshTwo  ()  const   noexcept    { return m_dimensions[1]->getMesh(); }

        inline auto&    getCurent   ()  const   noexcept    { return m_current_dimension; }

    private:

        synk::engine::EngineConstObserverPtr            m_engine;
        synk::entities::EntityManagerObserverPtr        m_world;

        synk::engine::KeyboardConstObserverPtr          m_keyboard;
        synk::engine::MouseConstObserverPtr             m_mouse;

        glm::vec2                                       m_size;
        std::array<DimensionOwnedPtr, 2>                m_dimensions;
        bool                                            m_current_dimension =   false;
};
