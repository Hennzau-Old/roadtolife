#pragma once

////////////////////
////////////////////
#include <GL/glew.h>
#include <string>

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
namespace synk::engine
{
    class Texture : public storm::core::NonCopyable
    {
        public:

            Texture (const std::string& image,
                     const bool& alpha = true);

            Texture (const unsigned char* buffer_image,
                     const std::uint32_t& width,
                     const std::uint32_t& height,
                     const bool& alpha = true);

            Texture (const GLuint& ID);

            ~Texture();

            void    bind    (GLenum index   =   GL_TEXTURE0)    const   noexcept;
            void    unbind  ()                                  const   noexcept;

        private:

            GLuint  m_ID;
    };
}
