#pragma once

#undef max
#undef min
#include <entt/entt.hpp>
#include <storm/core/Memory.hpp>

namespace synk::entities
{
    class EntityManager;
    DECLARE_PTR_AND_REF(EntityManager)
}
