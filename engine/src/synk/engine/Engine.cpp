#include <synk/engine/Engine.hpp>
#include <synk/engine/inputs/Keyboard.hpp>
#include <synk/engine/inputs/Mouse.hpp>
#include <synk/engine/components/DefaultComponents.hpp>
#include <synk/engine/scene/Scene.hpp>
#include <synk/engine/guis/GUIManager.hpp>
#include <synk/engine/Window.hpp>

////////////////////
////////////////////
#include <synk/entities/EntityManager.hpp>

////////////////////
////////////////////
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

////////////////////
using namespace synk::engine;

////////////////////
Engine::Engine(const Window::CreateInfo& window_settings)
{
    m_window    =   std::make_unique<Window>    (window_settings);
    m_keyboard  =   std::make_unique<Keyboard>  (*m_window);
    m_mouse     =   std::make_unique<Mouse>     (*m_window);
}

Engine::~Engine()
{

}

void Engine::update()   noexcept
{
    m_window->update();
    m_keyboard->update();
    m_mouse->update();
}

synk::entities::EntityManagerOwnedPtr Engine::createEntityManager()   const   noexcept
{
    return std::make_unique<synk::entities::EntityManager>();
}

synk::entities::EntityManagerOwnedPtr Engine::createDefaultEntityManager() const noexcept
{
    auto result { std::make_unique<synk::entities::EntityManager> () };

    result->addUpdateSystem([](entt::registry& registry)
    {
        const auto  view    { registry.view<DefaultsComponents::Position, DefaultsComponents::Velocity> () };

        for (const auto& entity : view)
        {
            auto&           pos { view.get<DefaultsComponents::Position> (entity) };
            auto&           vel { view.get<DefaultsComponents::Velocity> (entity) };

            const auto& temp_view   { registry.view<DefaultsComponents::Collision, DefaultsComponents::Size> () };
            const auto  it          { temp_view.find(entity) };

            if (it != temp_view.end())
            {
                const auto& collision   { temp_view.get<DefaultsComponents::Collision> (*it) };
                const auto& size        { temp_view.get<DefaultsComponents::Size> (*it) };

                if (collision.value)
                {
                    const auto  xStep   { static_cast<std::uint32_t> (glm::abs(vel.x * 100)) };
                    for (auto i { 0u }; i < xStep; i++)
                    {
                        if (!collision.function(pos, { vel.x / xStep, 0, 0 }, size))
                        {
                            pos.x += vel.x / xStep;
                        } else
                        {
                            vel.x = 0;
                            break;
                        }
                    }

                    const auto  yStep   { static_cast<std::uint32_t> (glm::abs(vel.y * 100)) };
                    for (auto i { 0u }; i < yStep; i++)
                    {
                        if (!collision.function(pos, { 0, vel.y / yStep, 0 }, size))
                        {
                            pos.y += vel.y / yStep;
                        } else
                        {
                            vel.y = 0;
                            break;
                        }
                    }

                    const auto  zStep   { static_cast<std::uint32_t> (glm::abs(vel.z * 100)) };
                    for (auto i { 0u }; i < zStep; i++)
                    {
                        if (!collision.function(pos, { 0, 0, vel.z / zStep }, size))
                        {
                            pos.z += vel.z / zStep;
                        } else
                        {
                            vel.z = 0;
                            break;
                        }
                    }
                } else
                {
                    pos.x += vel.x;
                    pos.y += vel.y;
                    pos.z += vel.z;
                }
            } else
            {
                pos.x += vel.x;
                pos.y += vel.y;
                pos.z += vel.z;
            }
        }
    });

    result->addUpdateSystem([](entt::registry& registry)
    { 
        registry.view<DefaultsComponents::Velocity, DefaultsComponents::Inertia>().each([](auto& vel, auto& inertia)
        {
            vel.x    *=  inertia.coeff;
            vel.y    *=  inertia.coeff;
            vel.z    *=  inertia.coeff;

            if (vel.x > 0 && vel.x < 0.001f)    vel.x   =   0;
            if (vel.y > 0 && vel.y < 0.001f)    vel.y   =   0;
            if (vel.z > 0 && vel.z < 0.001f)    vel.z   =   0;

            if (vel.x < 0 && vel.x > -0.001f)   vel.x   =   0;
            if (vel.y < 0 && vel.y > -0.001f)   vel.y   =   0;
            if (vel.z < 0 && vel.z > -0.001f)   vel.z   =   0;
        });
    });

    result->addUpdateSystem([](entt::registry& registry)
    {
       registry.view<DefaultsComponents::Position, DefaultsComponents::Rotation, DefaultsComponents::Model>().each([](auto& pos, auto& rot, auto& model)
       {
            model.matrix    =   glm::mat4(1.0);
            model.matrix    =   glm::translate(model.matrix, glm::vec3(pos.x, pos.y, pos.z));

            model.matrix    =   glm::rotate(model.matrix, glm::radians(rot.x), glm::vec3(1, 0, 0));
            model.matrix    =   glm::rotate(model.matrix, glm::radians(rot.y), glm::vec3(0, 1, 0));
            model.matrix    =   glm::rotate(model.matrix, glm::radians(rot.z), glm::vec3(0, 0, 1));
       });
    });

    result->addUpdateSystem([](entt::registry& registry)
    {
       registry.view<DefaultsComponents::Position, DefaultsComponents::Rotation, DefaultsComponents::Camera>().each([](auto& pos, auto& rot, auto& cam)
       {
            auto        front       { glm::vec3() };
            const auto  up          { glm::vec3(0, 1, 0) };
            const auto  position    { glm::vec3(pos.x, pos.y, pos.z) };

            front.x = glm::cos(glm::radians(rot.x)) * glm::cos(glm::radians(rot.y));
            front.y = glm::sin(glm::radians(rot.y));
            front.z = glm::sin(glm::radians(rot.x)) * glm::cos(glm::radians(rot.y));

            front   = glm::normalize(front);

            cam.view = glm::lookAt(position, position + front, up);
       });
    });

    result->addUpdateSystem([](entt::registry& registry)
    {
        registry.view<DefaultsComponents::Position, DefaultsComponents::Velocity, DefaultsComponents::Rotation, DefaultsComponents::Controllable>().each([](auto& pos, auto& vel, auto& rot, auto& control)
        {
            if (control.value)
            {
                control.function(pos, vel, rot);
            }
        });
    });


    return result;
}

SceneOwnedPtr Engine::createScene(entities::EntityManager &entity_manager, const std::uint32_t &camera_binding, const std::uint32_t &model_binding)    const   noexcept
{
    return  std::make_unique<Scene>     (entity_manager, camera_binding, model_binding);
}

entt::entity Engine::createCamera(entities::EntityManager &entity_manager, const glm::vec3 &pos, const glm::vec3 &rot)  const    noexcept
{
    const auto  camera  { entity_manager.make() };

    entity_manager.addComponent<DefaultsComponents::Position>   (camera, pos.x, pos.y, pos.z);
    entity_manager.addComponent<DefaultsComponents::Velocity>   (camera, 0.0f, 0.0f, 0.0f);
    entity_manager.addComponent<DefaultsComponents::Rotation>   (camera, rot.x, rot.y, rot.z);

    return camera;
}

entt::entity Engine::create2DCamera(entities::EntityManager &entity_manager,
                                    const glm::vec3& pos, const glm::vec3& rot)  const    noexcept
{
    const auto  camera  { createCamera(entity_manager, pos, rot) };

    const auto  projection  { glm::ortho(0.0f, static_cast<float> (m_window->window_info.width), static_cast<float> (m_window->window_info.height), 0.0f, -1.0f, 1.0f) };

    entity_manager.addComponent<DefaultsComponents::Camera> (camera, projection, glm::mat4(1.0f));

    return camera;
}

entt::entity Engine::createStaticMesh(entities::EntityManager &entity_manager, const std::vector<float> &vertices, const std::vector<unsigned int> &indices, const std::vector<std::uint8_t>& data_formats, const glm::vec3 &pos, const glm::vec3 &rot)    const   noexcept
{
    const auto  static_mesh { entity_manager.make() };

    entity_manager.addComponent<DefaultsComponents::Position> (static_mesh, pos.x, pos.y, pos.z);
    entity_manager.addComponent<DefaultsComponents::Rotation> (static_mesh, rot.x, rot.y, rot.z);
    entity_manager.addComponent<DefaultsComponents::Model>    (static_mesh);
    entity_manager.addComponent<DefaultsComponents::StaticRenderObject>   (static_mesh);

    auto* render_object =   &entity_manager.get<DefaultsComponents::StaticRenderObject> (static_mesh);
    render_object->mesh  =   std::make_unique<StaticMesh> (vertices, indices, data_formats);

    return static_mesh;
}

GUIManagerOwnedPtr Engine::createGUIManager(const std::uint32_t &camera_binding, const std::uint32_t &model_binding)  const   noexcept
{
    return std::make_unique<GUIManager> (*this, camera_binding, model_binding);
}

