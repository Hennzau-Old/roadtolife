#pragma once

#include <array>

class Tile
{
    public:

        static constexpr auto   WIDTH   { 4 };
        static constexpr auto   HEIGHT  { 4 };
        static constexpr auto   SIZE    { 64 };

        enum class TILE_TYPE
        {
            TILE_NO,
            TILE_GREEN,
            TILE_YELLOW,
            TILE_PURPLE,
            TILE_DARK_BLUE,
            TILE_CYAN,
            TILE_COUNT
        };

        struct TileStructure
        {
            TILE_TYPE   type;
            bool        collision   =   false;
        };

        static std::array<float, 4 * 4>    getSquareData(const float& x, const float& y, const TILE_TYPE& tile)
        {
            const auto  tx  { static_cast<int> (tile) % WIDTH };
            const auto  ty  { static_cast<int> (tile) / WIDTH };

            return
            {
                x,          y,              (0.0f + tx) / WIDTH, (0.0f + ty) / HEIGHT,
                x + SIZE,   y,              (1.0f + tx) / WIDTH, (0.0f + ty) / HEIGHT,
                x + SIZE,   y + SIZE,       (1.0f + tx) / WIDTH, (1.0f + ty) / HEIGHT,
                x,          y + SIZE,       (0.0f + tx) / WIDTH, (1.0f + ty) / HEIGHT,
            };
        }
};
