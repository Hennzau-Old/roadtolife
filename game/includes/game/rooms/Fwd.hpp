#pragma once

#include <storm/core/Memory.hpp>

class Room;
DECLARE_PTR_AND_REF(Room)

class Dimension;
DECLARE_PTR_AND_REF(Dimension)

class RoomManager;
DECLARE_PTR_AND_REF(RoomManager)

class Builder;
DECLARE_PTR_AND_REF(Builder)
