#pragma once

#include <storm/core/NonCopyable.hpp>

#include <synk/engine/Fwd.hpp>
#include <synk/engine/scene/Fwd.hpp>
#include <synk/engine/inputs/Fwd.hpp>

#include <synk/entities/Fwd.hpp>

#include <game/rooms/Fwd.hpp>

class Builder : public storm::core::NonCopyable
{
    public:

        Builder (const synk::engine::Engine&    engine,
                 synk::engine::Scene&           scene,
                 RoomManager&                   room_manager);
        ~Builder();

        void    update  ()  noexcept;

    private:

        synk::engine::EngineConstObserverPtr        m_engine;
        synk::engine::SceneObserverPtr              m_scene;
        synk::entities::EntityManagerObserverPtr    m_world;
        synk::engine::KeyboardConstObserverPtr      m_keyboard;
        synk::engine::MouseConstObserverPtr         m_mouse;

        RoomManagerObserverPtr                      m_rooms;
};
