#include <synk/engine/inputs/Keyboard.hpp>
#include <synk/engine/Window.hpp>

////////////////////
using namespace synk::engine;

bool keys[65536];

Keyboard::Keyboard(const Window& window)
:
    m_window    { &window }
{
    glfwSetKeyCallback(m_window->getGLFWWindow(), keyCallback);
}

Keyboard::~Keyboard()
{

}

bool Keyboard::contain(const std::vector<int>& keys, const int& key)    const   noexcept
{
    return std::find(keys.begin(), keys.end(), key) != keys.end();
}

void Keyboard::update() noexcept
{
    m_up_keys.clear();

    for (auto i { 0u }; i < KEY_LAST; i++)
    {
        if (!getKey(i) && contain(m_current_keys, i))
        {
            m_up_keys.emplace_back(i);
        }
    }

    m_down_keys.clear();

    for (auto i { 0u }; i < KEY_LAST; i++)
    {
        if (getKey(i) && !contain(m_current_keys, i))
        {
            m_down_keys.emplace_back(i);
        }
    }

    m_current_keys.clear();

    for (auto i { 0u }; i < KEY_LAST; i++)
    {
        if (getKey(i))
        {
            m_current_keys.emplace_back(i);
        }
    }
}

bool Keyboard::getKey(const int& key) const noexcept
{
    return keys[key];
}

bool Keyboard::getKeyDown(const int& key) const noexcept
{
    return contain(m_down_keys, key);
}

bool Keyboard::getKeyUp(const int& key) const noexcept
{
    return contain(m_up_keys, key);
}

void Keyboard::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    keys[key]   =   action  != RELEASE;
}
