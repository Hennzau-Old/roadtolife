#pragma once

////////////////////
////////////////////
#include <GL/glew.h>
#include <string>
#include <vector>

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
////////////////////
#include <synk/engine/scene/Fwd.hpp>

////////////////////
namespace synk::engine
{
    class Shaders : public storm::core::NonCopyable
    {
        public:

            Shaders (const std::string& vertex_file,
                     const std::string& fragment_file);

            ~Shaders();

            void    bindUniformBuffer   (const std::string& name,
                                         const std::uint32_t& binding) const   noexcept;

            void    bindUniformBuffer   (const UniformBuffer& uniform_buffer)  const   noexcept;

            void    bind                ()  const   noexcept;
            void    unbind              ()  const   noexcept;

            inline auto getID           ()  const   noexcept    { return m_ID; }

        private:

            GLuint  m_ID;
    };
}
