#version 420 core

layout (location = 0) in vec2 	in_position;
layout (location = 1) in float 	in_color;
layout (location = 2) in vec2 	in_coords;

layout (std140, binding = 0) uniform Camera
{
    mat4    projection;
    mat4    view;
};

layout (std140, binding = 1) uniform Model
{
    mat4    model;
};

out float	pass_color;
out vec2    	pass_coords;

void main()
{
    pass_coords =   in_coords;
    pass_color  =   in_color;

    gl_Position = projection * view * model * vec4(in_position, 0.0, 1.0);
}

