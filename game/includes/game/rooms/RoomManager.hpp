#pragma once

#include <storm/core/NonCopyable.hpp>

#include <synk/engine/Fwd.hpp>
#include <synk/engine/scene/Fwd.hpp>
#include <synk/engine/inputs/Fwd.hpp>
#include <synk/engine/components/DefaultComponents.hpp>

#include <synk/entities/Fwd.hpp>

#include <synk/utils/Animator.hpp>

#include <game/rooms/Fwd.hpp>

#include <glm/glm.hpp>

class RoomManager : public storm::core::NonCopyable
{
    public:

        RoomManager (const synk::engine::Engine&    engine,
                     synk::engine::Scene&           scene);
        ~RoomManager();

        void    next    ()  noexcept;
        void    previous()  noexcept;

        void    stop    ()  noexcept;
        void    start   ()  noexcept;

        void    update  ()  noexcept;

        void    changeMode  (const bool& value)  noexcept;

        bool    collide (const glm::vec2&       pos,
                         const glm::vec2&       vel,
                         const glm::vec2&       size)  const   noexcept;

        bool    collide (const bool&            dimension,
                         const glm::vec2&       pos,
                         const glm::vec2&       vel,
                         const glm::vec2&       size)  const   noexcept;

        inline auto&    getCurrentRoom  ()          noexcept    { return *m_rooms[m_current_room]; }
        inline auto&    getCurrentRoom  ()  const   noexcept    { return *m_rooms[m_current_room]; }

        inline auto&    getPlayer       ()          noexcept    { return m_player; }
        inline auto&    getPlayer       ()  const   noexcept    { return m_player; }

    private:

        std::vector<RoomOwnedPtr>                   m_rooms;
        std::uint32_t                               m_current_room;

        synk::engine::EngineConstObserverPtr        m_engine;
        synk::engine::SceneObserverPtr              m_scene;
        synk::entities::EntityManagerObserverPtr    m_world;
        synk::engine::KeyboardConstObserverPtr      m_keyboard;
        synk::engine::MouseConstObserverPtr         m_mouse;

        synk::engine::ShadersOwnedPtr               m_shaders;
        synk::engine::TextureOwnedPtr               m_room_texture;

        BuilderOwnedPtr                             m_builder;
        bool                                        m_builder_status    =   false;

        // PLAYER //

        entt::entity                                m_player;
        std::unique_ptr<synk::utils::Animator>      m_player_animator;
        synk::engine::TextureOwnedPtr               m_player_texture;

        float                                       m_player_speed  =   1;

        bool                                        m_status    =   false;
};
