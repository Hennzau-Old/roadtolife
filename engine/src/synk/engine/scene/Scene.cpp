#include <synk/engine/scene/Scene.hpp>
#include <synk/engine/scene/StaticMesh.hpp>
#include <synk/engine/scene/Shaders.hpp>
#include <synk/engine/scene/Texture.hpp>
#include <synk/engine/scene/UniformBuffer.hpp>
#include <synk/engine/components/DefaultComponents.hpp>

////////////////////
////////////////////
#include <synk/entities/EntityManager.hpp>

////////////////////
////////////////////
#include <glm/glm.hpp>

////////////////////
////////////////////
#include <storm/log/LogHandler.hpp>

////////////////////
using namespace synk::engine;

////////////////////
using storm::log::operator""_module;
static constexpr auto LOG_MODULE    =   "Scene"_module;

////////////////////
////////////////////
Scene::Scene(entities::EntityManager& world, const std::uint32_t& camera_binding, const std::uint32_t& model_binding)
:
    m_world             { &world }
{
    m_camera_buffer =   std::make_unique<UniformBuffer> ("Camera", 2 * sizeof(glm::mat4), camera_binding);
    m_model_buffer  =   std::make_unique<UniformBuffer> ("Model", 1 * sizeof(glm::mat4), model_binding);
    m_model_buffer->upload<glm::mat4> (glm::mat4(1.0f), sizeof(glm::mat4), 0);
}

Scene::~Scene()
{

}

void Scene::setCamera(const entt::entity& camera)   noexcept
{
    m_camera_target =   camera;
}

void Scene::update() noexcept
{
    const auto& camera_infos    { m_world->get<DefaultsComponents::Camera> (m_camera_target) };
    m_camera_buffer->upload<glm::mat4> (camera_infos.projection, sizeof(camera_infos.projection), 0);
    m_camera_buffer->upload<glm::mat4> (camera_infos.view, sizeof(camera_infos.view), sizeof(camera_infos.projection));
}

void Scene::render()    const   noexcept
{
    for (const auto& i : m_static_meshes)
    {
        i.shaders->bind();

        for (const auto& j : i.objects)
        {
            j.texture->bind();

            for (const auto& k : j.meshes)
            {
                const auto& model   { m_world->get<DefaultsComponents::Model> (k) };
                m_model_buffer->upload<glm::mat4> (model.matrix, sizeof(glm::mat4), 0);

                const auto& mesh    { m_world->get<DefaultsComponents::StaticRenderObject> (k) };

                if (mesh.render)
                {
                    mesh.mesh->render();
                }
            }

            j.texture->unbind();
        }

        i.shaders->unbind();
    }
}

void Scene::addStaticMesh(const entt::entity& static_mesh, const Shaders& shaders, const Texture& texture, const bool& initialize_shaders)    noexcept
{
    using namespace storm::core;

    if (initialize_shaders)
    {
        shaders.bindUniformBuffer(*m_camera_buffer);
        shaders.bindUniformBuffer(*m_model_buffer);
    }

    auto    shader_found    { false };

    for (auto& render_object : m_static_meshes)
    {
        if (render_object.shaders  ==  makeConstObserver(shaders))
        {
            storm::log::LogHandler::ilog(LOG_MODULE, "Shader found -> complete it");

            shader_found    =   true;
            auto    texture_found   { false };

            for (auto& texture_object : render_object.objects)
            {
                if (texture_object.texture == makeConstObserver(texture))
                {
                    texture_found   =   true;
                    texture_object.meshes.push_back(static_mesh);
                }
            }

            if (!texture_found)
            {
                auto  texture_object  { TextureObjects {
                        .texture    { makeConstObserver(texture) },
                }};

                texture_object.meshes.push_back(static_mesh);

                render_object.objects.emplace_back(std::move(texture_object));
            }
        }
    }

    if (!shader_found)
    {
        storm::log::LogHandler::ilog(LOG_MODULE, "Shader not found -> create it");

        auto    render_object  { RenderObjects
        {
            .shaders    { makeConstObserver(shaders) }
        }};

        auto    texture_object  { TextureObjects
        {
            .texture    { makeConstObserver(texture) }
        }};

        texture_object.meshes.push_back(static_mesh);

        render_object.objects.emplace_back(std::move(texture_object));

        m_static_meshes.emplace_back(std::move(render_object));
    }
}

void Scene::removeStaticMesh(const entt::entity &static_mesh, const Shaders &shaders, const Texture &texture)   noexcept
{
    for (auto& mesh  :   m_static_meshes)
    {
        if (mesh.shaders == storm::core::makeObserver(shaders))
        {
            for (auto& render_object : mesh.objects)
            {
                if (render_object.texture == storm::core::makeConstObserver(texture))
                {
                    auto    it  { std::find(render_object.meshes.begin(), render_object.meshes.end(), static_mesh) };

                    if (it != render_object.meshes.end())
                    {
                        render_object.meshes.erase(it);
                    }
                }
            }
        }
    }
}

void Scene::removeStaticMesh(const entt::entity &static_mesh)   noexcept
{
    for (auto& mesh  :   m_static_meshes)
    {
        for (auto& render_object : mesh.objects)
        {
            auto    it  { std::find(render_object.meshes.begin(), render_object.meshes.end(), static_mesh) };

            if (it != render_object.meshes.end())
            {
                render_object.meshes.erase(it);
            }
        }
    }
}
