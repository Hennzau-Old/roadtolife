#include <synk/utils/Animator.hpp>

using namespace synk::utils;

Animator::Animator(const std::uint32_t& length, const std::uint32_t& speed, const bool& loop)
:
    m_length    { length },
    m_speed     { speed },
    m_frame     { 0 },
    m_loop      { loop }
{

}

Animator::~Animator()
{

}
