#pragma once

////////////////////
////////////////////
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <vector>

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
////////////////////
#include <synk/engine/Fwd.hpp>

////////////////////
namespace synk::engine
{
    class Mouse : public storm::core::NonCopyable
    {
        public:

            Mouse   (const Window& window);
            ~Mouse  ();

            bool    contain             (const std::vector<int>& buttons,
                                         const int& button)  const   noexcept;

            void    update              ()  noexcept;

            bool    getButton           (const int& button) const   noexcept;
            bool    getButtonDown       (const int& button) const   noexcept;
            bool    getButtonUp         (const int& button) const   noexcept;

            inline bool     isGrabed    ()  const   noexcept    { return m_grabed; }

            inline auto     getPos      ()  const   noexcept    { return m_current_pos; }
            inline auto     getVel      ()  const   noexcept    { return m_mouse_vel; }

            static void buttonCallback  (GLFWwindow* window,
                                         int button,
                                         int action,
                                         int mods);

            static void positionCallback(GLFWwindow* window,
                                         double xpos,
                                         double ypos);

        private:

            WindowConstObserverPtr  m_window;

            std::vector<int>        m_current_buttons,
                                    m_down_buttons,
                                    m_up_buttons;

            glm::vec2               m_current_pos;
            glm::vec2               m_mouse_vel;

            bool                    m_grabed    =   false;
    };
}
