#include <synk/engine/scene/Shaders.hpp>
#include <synk/engine/scene/UniformBuffer.hpp>

///////////////////////
///////////////////////
#include <storm/log/LogHandler.hpp>

///////////////////////
///////////////////////

#include <synk/utils/File.hpp>
///////////////////////
///////////////////////
using namespace synk::engine;

///////////////////////
using storm::log::operator""_module;
static constexpr auto LOG_MODULE    =   "Shaders"_module;

////////////////////
Shaders::Shaders(const std::string& vertex_file, const std::string& fragment_file)
{
    const   auto    makeShader  { [](const GLuint& ID, const std::string& code)
    {
        auto    result          { GL_FALSE };
        auto    info_log_length { 0 };

        const auto  code_ptr    { code.c_str() };

        glShaderSource(ID, 1, &code_ptr, nullptr);
        glCompileShader(ID);

        glGetShaderiv(ID, GL_COMPILE_STATUS, &result);
        glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &info_log_length);

        if (info_log_length > 0)
        {
            auto    shader_error_message    { std::vector<char> (info_log_length + 1) };
            glGetShaderInfoLog(ID, info_log_length, nullptr, shader_error_message.data());

            storm::log::LogHandler::flog(LOG_MODULE, "Compile Error : {}", shader_error_message.data());
        }
    }};

    const auto  vertex_ID   { glCreateShader(GL_VERTEX_SHADER) };
    const auto  fragment_ID { glCreateShader(GL_FRAGMENT_SHADER) };

    makeShader(vertex_ID,   synk::utils::File::readFile(vertex_file));
    makeShader(fragment_ID, synk::utils::File::readFile(fragment_file));

    m_ID    =   glCreateProgram();

    glAttachShader(m_ID, vertex_ID);
    glAttachShader(m_ID, fragment_ID);

    glLinkProgram(m_ID);

    auto    result          { GL_FALSE };
    auto    info_log_length { 0 };

    glGetProgramiv(m_ID, GL_LINK_STATUS, &result);
    glGetProgramiv(m_ID, GL_INFO_LOG_LENGTH, &info_log_length);

    if (info_log_length > 0)
    {
        auto    shader_error_message    { std::vector<char> (info_log_length + 1) };
        glGetShaderInfoLog(m_ID, info_log_length, nullptr, shader_error_message.data());

        storm::log::LogHandler::flog(LOG_MODULE, "Link Error : {}", shader_error_message.data());
    }

    glDetachShader(m_ID, vertex_ID);
    glDetachShader(m_ID, fragment_ID);

    glDeleteShader(vertex_ID);
    glDeleteShader(fragment_ID);
}

Shaders::~Shaders()
{
    glDeleteProgram(m_ID);
}

void Shaders::bindUniformBuffer(const std::string& name, const std::uint32_t& binding)    const   noexcept
{
    const auto  block_index { glGetUniformBlockIndex(m_ID, name.c_str()) };

    glUniformBlockBinding(m_ID, block_index, binding);
}

void Shaders::bindUniformBuffer(const UniformBuffer &uniform_buffer)    const   noexcept
{
    const auto  block_index { glGetUniformBlockIndex(m_ID, uniform_buffer.getName().c_str()) };

    glUniformBlockBinding(m_ID, block_index, uniform_buffer.getBinding());
}

void Shaders::bind()    const   noexcept
{
    glUseProgram(m_ID);
}

void Shaders::unbind()  const   noexcept
{
    glUseProgram(m_ID);
}
