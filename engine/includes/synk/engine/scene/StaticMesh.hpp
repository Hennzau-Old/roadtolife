#pragma once

////////////////////
////////////////////
#include <GL/glew.h>
#include <vector>

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
namespace synk::engine
{
    class StaticMesh : public storm::core::NonCopyable
    {
        public:

            StaticMesh  (const std::vector<float>& vertices,
                         const std::vector<unsigned int>& indices,
                         const std::vector<std::uint8_t>& data_formats);

            StaticMesh  (const StaticMesh& static_mesh);

            ~StaticMesh ();

            void            update      (const std::vector<float>& vertices,
                                         const std::uint32_t& offset) const   noexcept;

            void            update      (const std::vector<unsigned int>& indices,
                                         const std::uint32_t& offset) noexcept;

            void            render      ()    const   noexcept;

            inline auto&    getVertices ()  const   noexcept    { return m_vertices; }
            inline auto&    getIndices  ()  const   noexcept    { return m_indices; }
            inline auto&    getFormats  ()  const   noexcept    { return m_data_formats; }

        private:

            GLuint          m_vao,
                            m_vbo,
                            m_ibo;

            std::vector<float>          m_vertices;
            std::vector<unsigned int>   m_indices;
            std::vector<std::uint8_t>   m_data_formats;

            std::uint32_t   m_vertex_count;
    };
}
