#include <synk/engine/scene/Texture.hpp>

///////////////////////
///////////////////////
#define STB_IMAGE_IMPLEMENTATION
#include <synk/engine/stb_image.hpp>

///////////////////////
///////////////////////
#include <storm/log/LogHandler.hpp>

///////////////////////
using namespace synk::engine;

///////////////////////
using storm::log::operator""_module;
static constexpr auto LOG_MODULE    =   "Texture"_module;

////////////////////
Texture::Texture(const std::string& image, const bool& alpha)
{
    glGenTextures(1, &m_ID);

    auto width      { 0 };
    auto height     { 0 };
    auto channel    { 0 };

    const auto buffer_image { static_cast<unsigned char*> (stbi_load(image.c_str(), &width, &height, &channel, 0)) };

    if (!buffer_image)
    {
        storm::log::LogHandler::elog(LOG_MODULE, "Texture {} can't be load", image);

        glBindTexture(GL_TEXTURE_2D, m_ID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        const float pixels[] =
        {
              1, 0, 1,  1, 0, 1,
              1, 0, 1,  1, 0, 1,
        };

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_FLOAT, pixels);
    } else
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_ID);

        if (alpha)  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE,    buffer_image);
        else        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,  width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,     buffer_image);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,   GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,   GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,       GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,       GL_CLAMP_TO_EDGE);
    }

    stbi_image_free(buffer_image);
}

Texture::Texture(const unsigned char* buffer_image, const std::uint32_t& width, const std::uint32_t& height, const bool& alpha)
{
    glGenTextures(1, &m_ID);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_ID);

    if (alpha)  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,    buffer_image);
    else        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,  width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,     buffer_image);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,   GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,   GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,       GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,       GL_CLAMP_TO_EDGE);
}

Texture::Texture(const GLuint& ID)
:
    m_ID    { ID }
{

}

Texture::~Texture()
{
    glDeleteTextures(1, &m_ID);
}

void Texture::bind(GLenum index)    const   noexcept
{
    glActiveTexture(index);
    glBindTexture(GL_TEXTURE_2D, m_ID);
}

void Texture::unbind()  const   noexcept
{
    glBindTexture(GL_TEXTURE_2D, 0);
}
