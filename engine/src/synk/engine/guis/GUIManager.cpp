#include <synk/engine/guis/GUIManager.hpp>
#include <synk/engine/guis/GUIComponents.hpp>

#include <synk/engine/Engine.hpp>
#include <synk/engine/scene/Scene.hpp>
#include <synk/engine/scene/StaticMesh.hpp>
#include <synk/engine/scene/Texture.hpp>
#include <synk/engine/scene/Shaders.hpp>
#include <synk/engine/inputs/Keyboard.hpp>
#include <synk/engine/inputs/Mouse.hpp>
#include <synk/engine/components/DefaultComponents.hpp>
#include <synk/engine/components/Primitives.hpp>

#include <synk/entities/EntityManager.hpp>
#include <synk/engine/Window.hpp>

#include <glm/glm.hpp>

using namespace synk::engine;

GUIManager::GUIManager(const Engine& engine,
                       const std::uint32_t& camera_binding,
                       const std::uint32_t& model_binding)
:
    m_engine    { &engine }
{
    m_keyboard  =   m_engine->getKeyboard();
    m_mouse     =   m_engine->getMouse();

    m_manager   =   m_engine->createDefaultEntityManager();
    m_scene     =   m_engine->createScene(*m_manager, camera_binding, model_binding);
    m_camera    =   m_engine->create2DCamera(*m_manager);
    m_scene->setCamera(m_camera);

    m_shaders               =   std::make_unique<Shaders> ("res/shaders/gui.vert", "res/shaders/gui.frag");
    m_slider_texture        =   std::make_unique<Texture> ("res/textures/guis/slider.png");
    m_check_box_texture     =   std::make_unique<Texture> ("res/textures/guis/check_box.png");
    m_not_check_box_texture =   std::make_unique<Texture> ("res/textures/guis/not_check_box.png");

    m_manager->addUpdateSystem([this](entt::registry& registry)
    {
        registry.view<DefaultsComponents::Position, GUIComponents::Button, GUIComponents::Status>().each([this](auto& pos, auto& button, auto& status)
        {
            if (status.status)
            {
                const auto  real_pos    { glm::vec2(pos.x, pos.y) * m_engine->getWindow()->getResizedFactor() };
                const auto  real_size   { glm::vec2(button.size.x, button.size.y) * m_engine->getWindow()->getResizedFactor() };

                if (m_mouse->getButton(0))
                {
                    if (m_mouse->getPos().x > real_pos.x && m_mouse->getPos().x < real_pos.x + real_size.x)
                    {
                        if (m_mouse->getPos().y > real_pos.y && m_mouse->getPos().y < real_pos.y + real_size.y)
                        {
                            button.clicked  =   true;
                        }
                    }

                } else
                {
                    button.clicked  =   false;
                }
            } else
            {
                button.clicked = false;
            }
        });
    });

    m_manager->addUpdateSystem([this](entt::registry& registry)
    {
       registry.view<DefaultsComponents::Position, GUIComponents::Button, GUIComponents::Slide, GUIComponents::Status>().each([this](auto& pos, auto& button, auto& slide, auto& status)
       {
           if (button.clicked && status.status)
           {
               const auto   min_pos_x   { (slide.pos.x - slide.rayon.x) * m_engine->getWindow()->getResizedFactor().x };
               const auto   max_pos_x   { (slide.pos.x + slide.rayon.x) * m_engine->getWindow()->getResizedFactor().x };
               const auto   min_pos_y   { (slide.pos.y - slide.rayon.y) * m_engine->getWindow()->getResizedFactor().y };
               const auto   max_pos_y   { (slide.pos.y + slide.rayon.y) * m_engine->getWindow()->getResizedFactor().y };

               if (m_mouse->getPos().x > min_pos_x - 50 && m_mouse->getPos().x < max_pos_x + 50 &&
                   m_mouse->getPos().y > min_pos_y - 50 && m_mouse->getPos().y < max_pos_y + 50)
               {
                   pos.x += (m_mouse->getVel().x) / m_engine->getWindow()->getResizedFactor().x;
                   pos.y += (m_mouse->getVel().y) / m_engine->getWindow()->getResizedFactor().y;
               }

               const auto   real_pos    { glm::vec2(pos.x, pos.y) * m_engine->getWindow()->getResizedFactor() };

               if (real_pos.x < min_pos_x)   pos.x = min_pos_x / m_engine->getWindow()->getResizedFactor().x;
               if (real_pos.x > max_pos_x)   pos.x = max_pos_x / m_engine->getWindow()->getResizedFactor().x;
               if (real_pos.y < min_pos_y)   pos.y = min_pos_y / m_engine->getWindow()->getResizedFactor().y;
               if (real_pos.y > max_pos_y)   pos.y = max_pos_y / m_engine->getWindow()->getResizedFactor().y;
           }
       });
    });

    m_manager->addUpdateSystem([this](entt::registry& registry)
    {
        auto    view    { registry.view<GUIComponents::Button, GUIComponents::CheckBox, GUIComponents::Status, DefaultsComponents::StaticRenderObject>() };

        for (auto& entity : view)
        {
            const auto& button  { view.get<GUIComponents::Button>                       (entity) };
            auto&       box     { view.get<GUIComponents::CheckBox>                     (entity) };
            const auto& status  { view.get<GUIComponents::Status>                       (entity) };

            if (status.status && button.clicked && !box.click_status)
            {
                m_scene->removeStaticMesh(entity);

                const auto  box_primitive   { Primitives::getSquare(glm::vec2(), glm::vec2(button.size.x, button.size.y), 1.0f, glm::vec4(0, 0, 1, 1)) };
                auto&   render_object   { view.get<DefaultsComponents::StaticRenderObject>  (entity) };
                render_object.mesh.reset(new StaticMesh(box_primitive.vertices, box_primitive.indices, box_primitive.data_formats));

                if (!box.check_status)
                {
                    m_scene->addStaticMesh(entity, *m_shaders, *m_check_box_texture);
                    box.check_status    =   true;
                } else
                {
                    m_scene->addStaticMesh(entity, *m_shaders, *m_not_check_box_texture);
                    box.check_status    =   false;
                }

                box.click_status    =   true;
            }

            if (!button.clicked)
            {
                box.click_status    =   false;
            }
        }
    });

    m_manager->addUpdateSystem([this](entt::registry& registry)
    {
        registry.view<DefaultsComponents::Position, GUIComponents::Button, GUIComponents::Overlable, DefaultsComponents::StaticRenderObject, GUIComponents::Status> ().each([this](auto& pos, auto& button, auto& overlable, auto& render_object, auto& status)
        {
            if (status.status)
            {
                const auto  real_pos    { glm::vec2(pos.x, pos.y) * m_engine->getWindow()->getResizedFactor() };
                const auto  real_size   { glm::vec2(button.size.x, button.size.y) * m_engine->getWindow()->getResizedFactor() };

                if (m_mouse->getPos().x > real_pos.x && m_mouse->getPos().x < real_pos.x + real_size.x &&
                    m_mouse->getPos().y > real_pos.y && m_mouse->getPos().y < real_pos.y + real_size.y)
                {
                    if (!overlable.status)
                    {
                        const auto  primitive   { Primitives::getSquare(glm::vec2(), glm::vec2(button.size.x, button.size.y), overlable.value, glm::vec4(0, 0, 1, 1)) };
                        render_object.mesh.reset(new StaticMesh(primitive.vertices, primitive.indices, primitive.data_formats));
                    }

                    overlable.status = true;
                } else
                {
                    if (overlable.status)
                    {
                        const auto  primitive   { Primitives::getSquare(glm::vec2(), glm::vec2(button.size.x, button.size.y), 1.0f, glm::vec4(0, 0, 1, 1)) };
                        render_object.mesh.reset(new StaticMesh(primitive.vertices, primitive.indices, primitive.data_formats));
                    }

                    overlable.status = false;
                }
            }
        });
    });
}

GUIManager::~GUIManager()
{

}

entt::entity GUIManager::createButton(const glm::vec2 &pos, const glm::vec2 &size, const Texture& texture) noexcept
{
    const auto  result  { m_manager->make() };

    const auto  button_primitive    { Primitives::getSquare(glm::vec2(), size, 1.0f, glm::vec4(0, 0, 1, 1)) };

    m_manager->addComponent<DefaultsComponents::Position>           (result, pos.x, pos.y, 0.0f);
    m_manager->addComponent<DefaultsComponents::Rotation>           (result, 0.0f, 0.0f, 0.0f);
    m_manager->addComponent<DefaultsComponents::Model>              (result);
    m_manager->addComponent<DefaultsComponents::StaticRenderObject> (result);
    m_manager->addComponent<GUIComponents::Button>                  (result, false, size);
    m_manager->addComponent<GUIComponents::Status>                  (result, false);
    m_manager->addComponent<GUIComponents::Overlable>               (result, 0.8f);

    auto& render_object { m_manager->get<DefaultsComponents::StaticRenderObject> (result) };
    render_object.mesh      =   std::make_unique<StaticMesh> (button_primitive.vertices, button_primitive.indices, button_primitive.data_formats);
    render_object.render    =   false;

    m_scene->addStaticMesh(result, *m_shaders, texture, true);

    return result;
}

entt::entity GUIManager::createSlider(const glm::vec2 &pos, const glm::vec2 &size, const glm::vec2 &rayon)  noexcept
{
    const auto  result  { m_manager->make() };

    const auto  slider_primitive    { Primitives::getSquare(glm::vec2(), size, 1.0f, glm::vec4(0, 0, 1, 1)) };

    m_manager->addComponent<DefaultsComponents::Position>           (result, pos.x, pos.y, 0.0f);
    m_manager->addComponent<DefaultsComponents::Rotation>           (result, 0.0f, 0.0f, 0.0f);
    m_manager->addComponent<DefaultsComponents::Model>              (result);
    m_manager->addComponent<DefaultsComponents::StaticRenderObject> (result);
    m_manager->addComponent<GUIComponents::Button>                  (result, false, size);
    m_manager->addComponent<GUIComponents::Slide>                   (result, rayon, glm::vec4(pos.x, pos.y, 0, 0));
    m_manager->addComponent<GUIComponents::Status>                  (result, false);
    m_manager->addComponent<GUIComponents::Overlable>               (result, 0.8f);

    auto& render_object { m_manager->get<DefaultsComponents::StaticRenderObject> (result) };
    render_object.mesh      =   std::make_unique<StaticMesh> (slider_primitive.vertices, slider_primitive.indices, slider_primitive.data_formats);
    render_object.render    =   false;

    m_scene->addStaticMesh(result, *m_shaders, *m_slider_texture, true);

    return result;
}

entt::entity GUIManager::createText(const std::string &text, const glm::vec2 &pos, const float& scale, const FontManager::Font &font)    noexcept
{
    const auto  result  { m_manager->make() };

    std::vector<float> vertices;
    std::vector<unsigned int> indices;


    std::vector<std::uint8_t> data_formats
    {
        2, 1, 2
    };

    auto    offset  { glm::vec2() };
    auto    count   { 0u };

    for (const auto& c : text)
    {
        if (c == ' ')
        {
            offset.x += 20 * scale;
            continue;
        }
        if (c == '\n')
        {
            offset.x = -20 * scale;
            offset.y -= 40 * scale;
            continue;
        }

        offset += FontManager::fill(c, font, scale, vertices, indices, offset, count);

        count++;
    }

    m_manager->addComponent<DefaultsComponents::Position>           (result, pos.x, pos.y, 0.0f);
    m_manager->addComponent<DefaultsComponents::Rotation>           (result, 0.0f, 0.0f, 0.0f);
    m_manager->addComponent<DefaultsComponents::Model>              (result);
    m_manager->addComponent<DefaultsComponents::StaticRenderObject> (result);
    m_manager->addComponent<GUIComponents::Text>                    (result, text, text);
    m_manager->addComponent<GUIComponents::Status>                  (result, false);

    auto& render_object { m_manager->get<DefaultsComponents::StaticRenderObject> (result) };
    render_object.mesh  =   std::make_unique<StaticMesh> (vertices, indices, data_formats);
    render_object.render    =   false;

    m_scene->addStaticMesh(result, *m_shaders, *font.texture_font, true);

    return result;
}

entt::entity GUIManager::createCheckBox(const glm::vec2 &pos, const glm::vec2 &size, const bool& status)    noexcept
{
    const auto  result  { m_manager->make() };

    const auto  box_primitive   { Primitives::getSquare(glm::vec2(), size, 1.0f, glm::vec4(0, 0, 1, 1)) };

    m_manager->addComponent<DefaultsComponents::Position>           (result, pos.x, pos.y, 0.0f);
    m_manager->addComponent<DefaultsComponents::Rotation>           (result, 0.0f, 0.0f, 0.0f);
    m_manager->addComponent<DefaultsComponents::Model>              (result);
    m_manager->addComponent<DefaultsComponents::StaticRenderObject> (result);
    m_manager->addComponent<GUIComponents::Button>                  (result, false, size);
    m_manager->addComponent<GUIComponents::CheckBox>                (result, status, false);
    m_manager->addComponent<GUIComponents::Status>                  (result, false);

    auto& render_object { m_manager->get<DefaultsComponents::StaticRenderObject> (result) };
    render_object.mesh      =   std::make_unique<StaticMesh> (box_primitive.vertices, box_primitive.indices, box_primitive.data_formats);
    render_object.render    =   false;

    if (!status)
    {
        m_scene->addStaticMesh(result, *m_shaders, *m_not_check_box_texture, true);
    } else
    {
        m_scene->addStaticMesh(result, *m_shaders, *m_check_box_texture, true);
    }

    return result;
}

entt::entity GUIManager::createBackground(const glm::vec2 &size, const Texture &texture)    noexcept
{
    const auto  result  { m_manager->make() };

    const auto  quad_primitive    { Primitives::getSquare(glm::vec2(), size, 1.0f, glm::vec4(0, 0, 1, 1)) };

    m_manager->addComponent<DefaultsComponents::Position>           (result, 0.0f, 0.0f, 0.0f);
    m_manager->addComponent<DefaultsComponents::Rotation>           (result, 0.0f, 0.0f, 0.0f);
    m_manager->addComponent<DefaultsComponents::Model>              (result);
    m_manager->addComponent<DefaultsComponents::StaticRenderObject> (result);
    m_manager->addComponent<GUIComponents::Status>                  (result, false);

    auto& render_object { m_manager->get<DefaultsComponents::StaticRenderObject> (result) };
    render_object.mesh      =   std::make_unique<StaticMesh> (quad_primitive.vertices, quad_primitive.indices, quad_primitive.data_formats);
    render_object.render    =   false;

    m_scene->addStaticMesh(result, *m_shaders, texture, true);

    return result;
}

bool GUIManager::isClicked(const entt::entity &button)  const   noexcept
{
    const auto& button_infos    { m_manager->get<GUIComponents::Button> (button) };

    return button_infos.clicked;
}

glm::vec2 GUIManager::getValue(const entt::entity &slider)  const   noexcept
{
    const auto& pos             { m_manager->get<DefaultsComponents::Position>  (slider) };
    const auto& slider_infos    { m_manager->get<GUIComponents::Slide>          (slider) };

    return glm::vec2(pos.x - slider_infos.pos.x, slider_infos.pos.y - pos.y);
}

bool GUIManager::isCheck(const entt::entity &check_box) const   noexcept
{
    const auto& check_box_infos { m_manager->get<GUIComponents::CheckBox> (check_box) };

    return check_box_infos.check_status;
}

void GUIManager::update()   noexcept
{
    m_manager->update();
    m_scene->update();
}

void GUIManager::render()   const   noexcept
{
    m_scene->render();
}

void GUIManager::stop(const Interface &_interface)   noexcept
{
    for (auto& entity : _interface.components)
    {
        auto&   render_object   { m_manager->get<DefaultsComponents::StaticRenderObject> (entity.second) };
        render_object.render    =   false;

        auto&   status_object   { m_manager->get<GUIComponents::Status> (entity.second) };
        status_object.status    =   false;
    }
}

void GUIManager::start(const Interface &_interface)   noexcept
{
    for (auto& entity : _interface.components)
    {
        auto&   render_object   { m_manager->get<DefaultsComponents::StaticRenderObject> (entity.second) };
        render_object.render    =   true;

        auto&   status_object   { m_manager->get<GUIComponents::Status> (entity.second) };
        status_object.status    =   true;
    }
}
