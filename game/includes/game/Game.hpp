#pragma once

////////////////////
////////////////////
#include <storm/core/NonCopyable.hpp>

////////////////////
////////////////////
#include <synk/engine/Fwd.hpp>
#include <synk/engine/inputs/Fwd.hpp>
#include <synk/engine/scene/Fwd.hpp>
#include <synk/engine/guis/Fwd.hpp>
#include <synk/engine/guis/GUIManager.hpp>
////////////////////
////////////////////
#include <synk/entities/Fwd.hpp>

////////////////////
////////////////////
#include <game/Fwd.hpp>
#include <game/rooms/Fwd.hpp>

////////////////////
class Game : public storm::core::NonCopyable
{
    public:

        enum class GAME_STATES
        {
            MAIN_MENU,
            OPTIONS_MENU,
            IN_GAME_MENU,
            GAME,
            NUM_MENUS
        };

        Game    ();
        ~Game   ();

        void    initMenus   ()  noexcept;

        void    update()            noexcept;
        void    render()    const   noexcept;

        void    manageGameStates()  noexcept;
        void    manageGameMenus ()  noexcept;

        void    setGameState    (const GAME_STATES& state)  noexcept;

        bool    isClosed    ()  const   noexcept;

        inline auto&    getWindow   ()          noexcept    { return m_window; }
        inline auto&    getWindow   ()  const   noexcept    { return m_window; }

    private:

        synk::engine::WindowObserverPtr         m_window;
        synk::engine::EngineOwnedPtr            m_engine;
        synk::engine::KeyboardObserverPtr       m_keyboard;
        synk::engine::MouseObserverPtr          m_mouse;

        GAME_STATES                             m_game_state;

        synk::engine::SceneOwnedPtr             m_scene;
        synk::engine::GUIManagerOwnedPtr        m_gui_manager;

        synk::entities::EntityManagerOwnedPtr   m_world;
        entt::entity                            m_camera;
        entt::entity                            m_player;

        RoomManagerOwnedPtr                     m_room_manager;

        // MENUS GESTION

        synk::engine::TextureOwnedPtr           m_start_button;
        synk::engine::TextureOwnedPtr           m_options_button;
        synk::engine::TextureOwnedPtr           m_resume_button;
        synk::engine::TextureOwnedPtr           m_quit_button;
        synk::engine::TextureOwnedPtr           m_main_background_texture;

        synk::engine::GUIManager::Interface     m_main_menu;
        synk::engine::GUIManager::Interface     m_options_menu;
        synk::engine::GUIManager::Interface     m_in_game_menu;

        bool                                    m_status    =   false;
};
