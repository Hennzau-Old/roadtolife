#include <synk/utils/ThreadPool.hpp>

using namespace synk::utils;

ThreadPool::ThreadPool(const std::uint32_t& count)
:
    m_count { count }
{
    m_mutex     =   std::make_unique<std::mutex> ();
    m_status    =   false;

    for (auto i { 0u }; i < m_count; i++)
    {
        m_workers.emplace_back(std::make_unique<std::thread>([this]()
        {
            while (true)
            {
                m_mutex->lock();

                if (!m_tasks.empty())
                {
                    const   auto    task    { m_tasks.front() };

                    m_tasks.pop();
                    m_mutex->unlock();

                    task();
                } else 
                {
                    m_mutex->unlock();

                    if (m_status)
                    {
                        break;
                    }
                }
            }
        }));
    }
}

ThreadPool::~ThreadPool()
{
    
}