#include <game/rooms/RoomManager.hpp>
#include <game/rooms/Room.hpp>

#include <synk/engine/Engine.hpp>
#include <synk/engine/scene/Scene.hpp>
#include <synk/engine/scene/Shaders.hpp>
#include <synk/engine/scene/Texture.hpp>
#include <synk/engine/inputs/Keyboard.hpp>
#include <synk/engine/inputs/Mouse.hpp>
#include <synk/engine/components/DefaultComponents.hpp>
#include <synk/engine/components/Primitives.hpp>

#include <synk/entities/EntityManager.hpp>

#include <game/rooms/Builder.hpp>

#include <storm/log/LogHandler.hpp>

RoomManager::RoomManager(const synk::engine::Engine&    engine,
                         synk::engine::Scene&           scene)
:
    m_engine    { &engine },
    m_scene     { &scene }
{
    m_keyboard  =   m_engine->getKeyboard();
    m_mouse     =   m_engine->getMouse();
    m_world     =   m_scene->getEntityManager();

    m_builder   =   std::make_unique<Builder> (*m_engine, *m_scene, *this);

    m_shaders       = std::make_unique<synk::engine::Shaders> ("res/shaders/main.vert", "res/shaders/main.frag");
    m_room_texture  = std::make_unique<synk::engine::Texture> ("res/textures/mini_tileset.png", false);

    for (auto i { 0u }; i < 3; i++)
    {
        auto  room    { std::make_unique<Room> (*m_engine, *m_world, glm::vec2(20, 11)) };

        m_scene->addStaticMesh(room->getMeshOne(), *m_shaders, *m_room_texture, true);
        m_scene->addStaticMesh(room->getMeshTwo(), *m_shaders, *m_room_texture, true);

        m_rooms.emplace_back(std::move(room));
    }

    m_current_room  =   0;
    m_rooms[m_current_room]->start();

    // Player ! //

    const auto  player_primitive    { synk::engine::Primitives::getSquare(glm::vec2(0), glm::vec2(Tile::SIZE), glm::vec4(0, 0, 6.0f, 4.0f)) };

    m_player_texture    =   std::make_unique<synk::engine::Texture> ("res/textures/world/newPlayerSprite.png");
    m_player_animator   =   std::make_unique<synk::utils::Animator> (6, 5, true);
    m_player            =   m_engine->createStaticMesh(*m_world, player_primitive.vertices, player_primitive.indices, player_primitive.data_formats);
    m_scene->addStaticMesh(m_player, *m_shaders, *m_player_texture);

    m_world->addComponent<synk::engine::DefaultsComponents::Velocity>       (m_player, 0.0f, 0.0f, 0.0f);
    m_world->addComponent<synk::engine::DefaultsComponents::Inertia>        (m_player, 0.75f);
    m_world->addComponent<synk::engine::DefaultsComponents::Size>           (m_player, static_cast<float> (Tile::SIZE), static_cast<float> (Tile::SIZE), 0.0f);
    m_world->addComponent<synk::engine::DefaultsComponents::Controllable>   (m_player);
    m_world->addComponent<synk::engine::DefaultsComponents::Collision>      (m_player);

    auto&   control { m_world->get<synk::engine::DefaultsComponents::Controllable> (m_player) };
    control.function    =   [this](auto& pos, auto& vel, auto& rot)
    {
        auto    dir { glm::vec2() };

        m_player_animator->pause();

        if (m_keyboard->getKey(KEY_D))
        {
            dir.x   +=  m_player_speed;
            m_player_animator->play(4);
        }

        if (m_keyboard->getKey(KEY_A))
        {
            dir.x   +=  -m_player_speed;
            m_player_animator->play(1);
        }

        if (m_keyboard->getKey(KEY_W))
        {
            dir.y   +=  -m_player_speed;
            m_player_animator->play(2);
        }

        if (m_keyboard->getKey(KEY_S))
        {
            dir.y   +=  m_player_speed;
            m_player_animator->play(3);
        }

        if (!m_player_animator->isPlaying())
        {
            m_player_animator->stop();
        }

        if (dir.x != 0 || dir.y != 0)
        {
            if (dir.x != 0)
            {
                vel.y   +=  dir.y / 1.5f;
            } else
            {
                vel.y   +=  dir.y;
            }

            if (dir.y != 0)
            {
                vel.x   +=  dir.x / 1.5f;
            } else
            {
                vel.x   +=  dir.x;
            }

            const auto  player_primitive    { synk::engine::Primitives::getSquare(glm::vec2(0), glm::vec2(Tile::SIZE), glm::vec4(m_player_animator->getCurrentFrame(), m_player_animator->getDir() - 1, 6.0f, 4.0f)) };

            auto&   render_object   { m_world->get<synk::engine::DefaultsComponents::StaticRenderObject> (m_player) };
            render_object.mesh->update(player_primitive.vertices, 0);
        }
    };

    auto& collision { m_world->get<synk::engine::DefaultsComponents::Collision> (m_player) };

    collision.function = [this](const auto& pos, const auto& vel, const auto& size)
    {
        return collide(glm::vec2(pos.x, pos.y), glm::vec2(vel.x, vel.y), glm::vec2(size.x, size.y));
    };

    m_status = true;
}

RoomManager::~RoomManager()
{
    for (const auto& room : m_rooms)
    {
        m_scene->removeStaticMesh(room->getMeshOne(), *m_shaders, *m_room_texture);
        m_scene->removeStaticMesh(room->getMeshTwo(), *m_shaders, *m_room_texture);
    }

    m_scene->removeStaticMesh(m_player, *m_shaders, *m_player_texture);

    m_world->removeAll(m_player);
    m_world->destroy(m_player);
}

void RoomManager::next()    noexcept
{
    if (m_current_room + 1 >= m_rooms.size())
    {
        return;
    }

    m_rooms[m_current_room]->stop();
    m_current_room++;
    m_rooms[m_current_room]->start();
}

void RoomManager::previous()    noexcept
{
    if (static_cast<int> (m_current_room) - 1 < 0)
    {
        return;
    }

    m_rooms[m_current_room]->stop();
    m_current_room--;
    m_rooms[m_current_room]->start();
}

void RoomManager::stop()    noexcept
{
    m_rooms[m_current_room]->stop();
    m_status    =   false;

    auto&   render_object   { m_world->get<synk::engine::DefaultsComponents::StaticRenderObject> (m_player) };
    render_object.render    =   false;

    auto&   player_object   { m_world->get<synk::engine::DefaultsComponents::Controllable> (m_player) };
    player_object.value =   false;
}

void RoomManager::start()   noexcept
{
    m_rooms[m_current_room]->start();
    m_status    =   true;

    auto&   render_object   { m_world->get<synk::engine::DefaultsComponents::StaticRenderObject> (m_player) };
    render_object.render    =   true;

    auto&   player_object   { m_world->get<synk::engine::DefaultsComponents::Controllable> (m_player) };
    player_object.value =   true;
}

void RoomManager::update()  noexcept
{
    if (!m_status)
    {
        return;
    }

    if (m_keyboard->getKeyDown(KEY_Q))
    {
        const auto& pos     { m_world->get<synk::engine::DefaultsComponents::Position>  (m_player) };
        const auto& vel     { m_world->get<synk::engine::DefaultsComponents::Velocity>  (m_player) };
        const auto& size    { m_world->get<synk::engine::DefaultsComponents::Size>      (m_player) };

        const auto& opposite    { !m_rooms[m_current_room]->getCurent() };

        if (!collide(opposite, glm::vec2(pos.x, pos.y), glm::vec2(0.0f), glm::vec2(size.x, size.y)))
        {
            if (!collide(opposite, glm::vec2(pos.x, pos.y), glm::vec2(vel.x, vel.y), glm::vec2(size.x, size.y)))
            {
                m_rooms[m_current_room]->swap();
            }
        }
    }

    if (m_builder_status)
    {
        m_builder->update();
    }

    m_player_animator->update();
}

void RoomManager::changeMode(const bool& value)  noexcept
{
    m_builder_status    =   value;
}

bool RoomManager::collide(const glm::vec2 &pos, const glm::vec2& vel, const glm::vec2 &size)  const   noexcept
{
    return collide(m_rooms[m_current_room]->getCurent(), pos, vel, size);
}

bool RoomManager::collide(const bool& dimension, const glm::vec2 &pos, const glm::vec2& vel, const glm::vec2 &size)  const   noexcept
{
    const auto  x   { pos.x   +   vel.x };
    const auto  y   { pos.y   +   vel.y };

    if (x < 0 || y < 0) return true;

    if (m_rooms[m_current_room]->getTile(dimension, (x + 14            )   / Tile::SIZE, (y + 25            )   / Tile::SIZE).collision) return true;
    if (m_rooms[m_current_room]->getTile(dimension, (x + size.x - 14   )   / Tile::SIZE, (y + 25            )   / Tile::SIZE).collision) return true;
    if (m_rooms[m_current_room]->getTile(dimension, (x + size.x - 14   )   / Tile::SIZE, (y + size.y        )   / Tile::SIZE).collision) return true;
    if (m_rooms[m_current_room]->getTile(dimension, (x + 14            )   / Tile::SIZE, (y + size.y        )   / Tile::SIZE).collision) return true;

    return false;
}
