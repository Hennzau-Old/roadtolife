#pragma once

#undef max
#undef min
#include <entt/entt.hpp>

#include <storm/core/NonCopyable.hpp>

namespace synk::entities
{
    class EntityManager : public storm::core::NonCopyable
    {
        public:

            EntityManager   ();
            ~EntityManager  ();

            void            update      ()  noexcept;
            void            render      ()  noexcept;

            template<typename T, typename... Args>
            inline void     addComponent(entt::entity entity, Args&&... args)   noexcept;
            inline auto     make        ()  noexcept;

            template<typename... T>
            inline auto     remove      (entt::entity)  noexcept;
            inline auto     removeAll   (entt::entity entity)  noexcept;
            inline auto     destroy     (entt::entity entity)  noexcept;

            template<typename T>
            inline auto&    get(entt::entity entity) const noexcept;
            template<typename T>
            inline auto&    get(entt::entity entity) noexcept;
            template<typename... T>
            inline auto     view() noexcept;

            inline void     addUpdateSystem (const std::function<void(entt::registry&)>& function) noexcept;
            inline void     addRenderSystem (const std::function<void(entt::registry&)>& function) noexcept;

            inline auto&    getRegistry ()          noexcept    { return m_registry; }
            inline auto&    getRegistry ()  const   noexcept    { return m_registry; }

        private:

            entt::registry                                          m_registry;
            std::vector<std::function<void(entt::registry&)>>       m_updates;
            std::vector<std::function<void(entt::registry&)>>       m_renders;
    };

    inline auto EntityManager::make()  noexcept
    {
        return m_registry.create();
    }

    template <typename... T>
    inline auto EntityManager::remove(entt::entity entity) noexcept
    {
        m_registry.remove<T...> (entity);
    }

    inline auto EntityManager::removeAll(entt::entity entity)   noexcept
    {
        m_registry.remove_all(entity);
    }

    inline auto EntityManager::destroy(entt::entity entity) noexcept
    {
        m_registry.destroy(entity);
    }

    template<typename T, typename...Args>
    inline void EntityManager::addComponent(entt::entity entity, Args&&... args) noexcept
    {
        m_registry.emplace<T>(entity, args...);
    }

    template<typename T>
    inline auto& EntityManager::get(entt::entity entity) const noexcept
    {
        return m_registry.get<T>(entity);
    }

    template<typename T>
    inline auto& EntityManager::get(entt::entity entity) noexcept
    {
        return m_registry.get<T>(entity);
    }

    template<typename... T>
    inline auto EntityManager::view() noexcept
    {
        return m_registry.view<T...>();
    }

    inline void EntityManager::addUpdateSystem(const std::function<void(entt::registry&)>& function) noexcept
    {
        m_updates.emplace_back(function);
    }

    inline void EntityManager::addRenderSystem(const std::function<void(entt::registry&)>& function) noexcept
    {
        m_renders.emplace_back(function);
    }
};
