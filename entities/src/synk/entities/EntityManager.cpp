#include <synk/entities/EntityManager.hpp>

using namespace synk::entities;

EntityManager::EntityManager()
{

}

EntityManager::~EntityManager()
{

}

void EntityManager::update() noexcept
{
    for (auto& update : m_updates)
    {
        update(m_registry);
    }
}

void EntityManager::render() noexcept
{
    for (auto& render : m_renders)
    {
        render(m_registry);
    }
}
