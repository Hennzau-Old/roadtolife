#include <game/rooms/Dimension.hpp>

#include <synk/engine/Engine.hpp>
#include <synk/engine/scene/StaticMesh.hpp>
#include <synk/engine/components/DefaultComponents.hpp>

#include <synk/entities/EntityManager.hpp>

#include <game/rooms/Tile.hpp>

Dimension::Dimension(const synk::engine::Engine& engine,
                     synk::entities::EntityManager& world,
                     const glm::vec2& size)
:
    m_engine    { &engine },
    m_world     { &world },
    m_size      { size }
{
    m_tiles.resize(m_size.x);

    for (auto x { 0u }; x < m_size.x; x++)
    {
        m_tiles[x].resize(m_size.y);

        for (auto y { 0u }; y < m_size.y; y++)
        {
            m_tiles[x][y]   =   { Tile::TILE_TYPE::TILE_CYAN, false };
        }
    }

    create();
}

Dimension::~Dimension()
{

}

void Dimension::create()    noexcept
{
    std::vector<float>          vertices;
    std::vector<unsigned int>   indices;

    auto    fillVertices    { [&vertices](const auto& array)
    {
        vertices.insert(vertices.end(), array.begin(), array.end());
    }};

    auto    fillIndices { [&indices](const auto& array, const auto& count)
    {
        for (const auto& v : array)
        {
            indices.emplace_back(v + 4 * count);
        }
    }};

    auto count  { 0u };

    std::array<unsigned int, 6>    indices_template
    {
        0, 1, 2,
        2, 3, 0
    };

    for (auto x { 0u }; x < m_size.x; x++)
    {
        for (auto y { 0u }; y < m_size.y; y++)
        {
            if (m_tiles[x][y].type == Tile::TILE_TYPE::TILE_NO)
            {
                continue;
            }

            const auto  xx  { x * Tile::SIZE };
            const auto  yy  { y * Tile::SIZE };

            fillVertices(Tile::getSquareData(xx, yy, m_tiles[x][y].type));
            fillIndices(indices_template, count);

            count++;
        }
    }

    std::vector<std::uint8_t> data_formats
    {
        2, 2
    };

    m_mesh  =   m_engine->createStaticMesh(*m_world, vertices, indices, data_formats);
}

void Dimension::update()    noexcept
{
    std::vector<float>          vertices;
    std::vector<unsigned int>   indices;

    auto    fillVertices    { [&vertices](const auto& array)
    {
        vertices.insert(vertices.end(), array.begin(), array.end());
    }};

    auto    fillIndices { [&indices](const auto& array, const auto& count)
    {
        for (const auto& v : array)
        {
            indices.emplace_back(v + 4 * count);
        }
    }};

    auto count  { 0u };

    std::array<unsigned int, 6>    indices_template
    {
        0, 1, 2,
        2, 3, 0
    };

    for (auto x { 0u }; x < m_size.x; x++)
    {
        for (auto y { 0u }; y < m_size.y; y++)
        {
            if (m_tiles[x][y].type == Tile::TILE_TYPE::TILE_NO)
            {
                continue;
            }

            const auto  xx  { x * Tile::SIZE };
            const auto  yy  { y * Tile::SIZE };

            fillVertices(Tile::getSquareData(xx, yy, m_tiles[x][y].type));
            fillIndices(indices_template, count);

            count++;
        }
    }

    std::vector<std::uint8_t> data_formats
    {
        2, 2
    };

    auto&   render_object   { m_world->get<synk::engine::DefaultsComponents::StaticRenderObject> (m_mesh) };
    render_object.mesh->update(vertices, 0);
    render_object.mesh->update(indices, 0);
}
